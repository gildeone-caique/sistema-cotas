<?php
include_once ("z_db.php");
include 'includes/mercadopago.php';
// Inialize session
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['paypalidsession'])) {
    redirect('404.php');
}
$util->maintain(3, 'O nosso sistema está em manutenção,por isso está desabilitado temporatiamente para novos cadastros.Tente novamente mais tarde.');

$userid = $_SESSION['paypalidsession'];

$usrNm = mysqli_real_escape_string($con, @$_SESSION['renova_usr']);
if (empty($usrNm)) {
    $usrNm = $userid;
}
$DB->where('username', $usrNm);
$data = $DB->getOne('affiliateuser');
$DB->where('id', $data['pcktaken']);
$pacoteInfo = $DB->getOne('packages');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $vencimento = date('Y-m-d', strtotime("+4 days"));
    $arr = ['pacote' => $pacoteInfo['id'], 'vencimento' => $vencimento, 'valor' => $pacoteInfo['price'], 'url' => '', 'status' => 1, 'ref' => 'Déposito/Transferência', 'username' => $userid, 'data_criacao' => date('Y-m-d'), 'descricao' => 'Ativação - ' . $pacoteInfo['name']];
    $util->insertPagamento($arr);
    redirect('finalthankyoufree.php?username=' . $userid);
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <style>html {
                overflow-y: scroll; 
            }</style>
        <meta charset="utf-8" />
        <title>Renove sua conta</title>
        <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->

    </head>
    <body style="overflow: scroll; background: url(images/background.png); max-width:100%" class="">
        <section id="content" class="m-t-lg wrapper-md animated fadeInDown">

            <div align=center> <a href="index.php"><img src="images/login.png" align="center" style="max-width:100%; padding-bottom:60px"></a>
                <section class="m-b-lg">
                    </br>
                    </br>
                    <?php
                    $mp = new mercadopago();
                    $urlMercado = $mp->pagamentoUrl($data, $pacoteInfo, 2);
                    ?>
                    <br>

                    <a  href="<?= $urlMercado ?>" class="btn btn-lg btn-primary btn-block"  >Pagar através do MercadoPago</a>
                    <br>
                    <?php
                    $query = "SELECT count(*) FROM  paymentgateway where name='Cash On Delivery' and status=1";
                    $result = mysqli_query($con, $query);
                    $row = mysqli_fetch_row($result);
                    $numrows = $row[0];

                    if ($numrows == 1) {
                        ?>
                        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">

                            <input type="submit" class="btn btn-lg btn-primary btn-block" value="Depósito/Transferência Bancária" >
                        </form>
                        <?php
                    }
                    ?>
            </div>
        </div>
    </section>
</div>
</section>
<!-- footer -->
<footer id="footer">
    <div class="text-center padder clearfix">
        <p> <small><?php
                $query = "SELECT footer from settings where sno=0";


                $result = mysqli_query($con, $query);

                while ($row = mysqli_fetch_array($result)) {
                    $footer = "$row[footer]";
                    print $footer;
                }
                ?></small> </p>
    </div>
</footer>
<!-- / footer -->
<!-- Bootstrap -->
<!-- App -->
<script src="js/app.v1.js"></script>
<script src="js/app.plugin.js"></script>
<script>alert("Você já consegiu 200% do valor do seu investimento e agora precisa renovar sua conta.");</script>
</body>
</html>