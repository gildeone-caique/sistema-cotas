<?php
include_once ("z_db.php");
// Inialize session
session_start();

// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['username'])) {
    redirect('index.php');
}


if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['todo'])) {


// Collect the data from post method of form submission // 
    $name = mysqli_real_escape_string($con, $_POST['fullname']);
    $address = mysqli_real_escape_string($con, $_POST['address']);
    $cont = mysqli_real_escape_string($con, $_POST['contry']);
    $p1 = mysqli_real_escape_string($con, $_POST['p1']);
    $p2 = mysqli_real_escape_string($con, $_POST['p2']);
    $bankname = mysqli_real_escape_string($con, $_POST['bankname']);
    $email = mysqli_real_escape_string($con, $_POST['email']);
    $accname = mysqli_real_escape_string($con, $_POST['accname']);
    $accno = mysqli_real_escape_string($con, $_POST['accno']);
    $ifsccode = mysqli_real_escape_string($con, $_POST['ifsccode']);
    $alwdpayment = mysqli_real_escape_string($con, $_POST['alwdpayment']);
    $acctype = mysqli_real_escape_string($con, $_POST['acctype']);
    $operacao = mysqli_real_escape_string($con, $_POST['operacao']);
//collection ends

    $check = 1;
    if ($check == 1) {

        $status = "OK";
        $msg = "";
//validation starts
// if userid is less than 6 char then status is not ok

        if ($p1 != '') {
            if (strlen($p1) < 8) {
                $msg = $msg . "Password Must Be More Than 8 Char Length.<BR>";
                $status = "NOTOK";
            }

            if (strlen($p2) < 8) {
                $msg = $msg . "Conformation Password Must Be More Than 8 Char Length.<BR>";
                $status = "NOTOK";
            }

            if ($p2 != $p1) {
                $msg = $msg . "Password Does Not Match.<BR>";
                $status = "NOTOK";
            }
        }



        if (strlen($address) < 5) {
            $msg = $msg . "address should contain 5 chars.<BR>";
            $status = "NOTOK";
        }
    }

    if ($status == "OK") {
        if ($p1 != '') {
            $query = mysqli_query($con, "update affiliateuser set password='$p1',address='$address',country='$cont',bankname='$bankname',operacao='$operacao',accountno='$accno',accounttype='$acctype',ifsccode='$ifsccode',email='$email',getpayment='$alwdpayment' where username='" . $_SESSION['username'] . "'");
        }else{
                        $query = mysqli_query($con, "update affiliateuser set address='$address',country='$cont',bankname='$bankname',operacao='$operacao',accountno='$accno',accounttype='$acctype',ifsccode='$ifsccode',email='$email',getpayment='$alwdpayment' where username='" . $_SESSION['username'] . "'");

        }

        $errormsg = "
<div class='alert alert-success'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <i class='fa fa-ban-circle'></i><strong>Success : </br></strong>Your profile has been updated.</div>"; //printing error if found in validation
    } else {
        $errormsg = "
<div class='alert alert-danger'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <i class='fa fa-ban-circle'></i><strong>Please Fix Below Errors : </br></strong>" . $msg . "</div>"; //printing error if found in validation
    }
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8" />
        <title><?= TITULO_DEF ?></title>
        <meta name="description" content="Sistema para Marketing Multinível" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->      
    </head>
    <body class="">
        <section class="vbox">
            <header class="bg-primary header header-md navbar navbar-fixed-top-xs box-shadow">
                <div class="navbar-header aside-md dk"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="fa fa-bars"></i> </a> <a href="dashboard.php" class="navbar-brand"><img src="images/logo.png" style="max-height:50px !important"><?php
$query = "SELECT header from settings where sno=0";


$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result)) {
    $header = "$row[header]";
    print $header;
}
?></a> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i> </a> </div>


                <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">

                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="images/a0.jpg"> </span> <?php
                        $sql = "SELECT fname FROM  affiliateuser WHERE username='" . $_SESSION['username'] . "'";
                        if ($result = mysqli_query($con, $sql)) {

                            /* fetch associative array */
                            while ($row = mysqli_fetch_row($result)) {
                                print $row[0];
                            }
                        }
?> <b class="caret"></b> </a>
                        <ul class="dropdown-menu animated fadeInRight">
                            <span class="arrow top"></span>
                            <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
                        </ul>
                    </li>
                </ul>
            </header>
            <section>
                <section class="hbox stretch">
                    <!-- .aside -->
                    <aside class="bg-light aside-md hidden-print" id="nav">
                        <section class="vbox">
                            <section class="w-f scrollable">
                                <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-color="#333333">
                                    <div class="clearfix wrapper dk nav-user hidden-xs">
                                        <div class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb avatar pull-left m-r"> <img src="images/a0.jpg"> <i class="on md b-black"></i> </span> <span class="hidden-nav-xs clear"> <span class="block m-t-xs"> <strong class="font-bold text-lt"><?php
                            $sql = "SELECT fname,country,pcktaken FROM  affiliateuser WHERE username='" . $_SESSION['username'] . "'";
                            if ($result = mysqli_query($con, $sql)) {

                                /* fetch associative array */
                                while ($row = mysqli_fetch_row($result)) {
                                    print $row[0];
                                    $coun = $row[1];
                                    $pcktaken = $row[2];
                                    $sql2 = "SELECT name FROM packages WHERE id=$pcktaken";
                                    if ($result2 = mysqli_query($con, $sql2)) {
                                        while ($row2 = mysqli_fetch_row($result2)) {

                                            $pkname = $row2[0];
                                        }
                                    }
                                }
                            }
?></strong> <b class="caret"></b> </span> <span class="text-muted text-xs block"><?php print "$pkname Member"; ?></span> </span> </a>
                                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                                <span class="arrow top hidden-nav-xs"></span>
                                                <li> <a href="profile.php">Perfil</a> </li>
                                                <li> <a href="notifications.php"> Notificações</a> </li>
                                                <li> <a href="contact.php">Suporte</a> </li>
                                                <li class="divider"></li>
                                                <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- nav -->
                                    <nav class="nav-primary hidden-xs">
<?php
include('includes/menu.php');
?>


                                    </nav>
                                    <!-- / nav -->
                                </div>
                            </section>
                            <footer class="footer hidden-xs no-padder text-center-nav-xs"> <a href="logout.php" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs"> <i class="i i-logout"></i> </a> <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs"> <i class="i i-circleleft text"></i> <i class="i i-circleright text-active"></i> </a> </footer>
                        </section>
                    </aside>
                    <!-- /.aside -->
                    <section id="content">
                        <section class="vbox">
                            <section class="scrollable wrapper">
                                <div class="row">

                                    <div class="col-sm-12 portlet">
                                        <section class="panel panel-success portlet-item">
                                            <header class="panel-heading"> General Settings </header>
                                            <ul class="nav nav-tabs nav-justified">
<?php
$query = "SELECT * FROM  affiliateuser WHERE username='" . $_SESSION['username'] . "'";


$result = mysqli_query($con, $query);
$i = 0;
while ($row = mysqli_fetch_array($result)) {

    $name = "$row[fname]";
    $add = "$row[address]";
    $contry = "$row[country]";
    $email = "$row[email]";
    $bname = "$row[bankname]";
    $accnamee = "$row[accountname]";
    $accnumber = "$row[accountno]";
    $acctyppe = "$row[accounttype]";
    $ifsc = "$row[ifsccode]";
    $operacao = "$row[operacao]";
    $cpf = "$row[cpf]";
}

$query121 = "SELECT * FROM  settings";


$result121 = mysqli_query($con, $query121);
$i = 0;
while ($row121 = mysqli_fetch_array($result121)) {


    $wlink = "$row121[wlink]";
}


$pathu = "/User/signup.php?aff=";
?>  
                                            </ul>
                                            </header>
                                            <div class="panel-body">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="home">
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST' && ($status != "")) {
    print $errormsg;
}
?>

                                                        <div class="panel-body">
                                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"], ENT_QUOTES, "utf-8"); ?>" method="post">

                                                                <div class="form-group">
                                                                    <input type="hidden" name="todo" value="post">
                                                                    <label>Link de indicação</label>
                                                                    <input type="text" value="<?php print $wlink . $pathu . $_SESSION['username'] ?>" class="form-control" placeholder="Link de indicação" name="inviteurl" >
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Nome completo</label>
                                                                    <input disabled="" type="text" value="<?php print $name ?>" class="form-control" placeholder="Nome completo" name="fullname" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Cpf</label>
                                                                    <input disabled="" type="text" value="<?php print $cpf ?>" class="form-control" placeholder="Nome completo" name="fullname" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Endereço</label>
                                                                    <input type="text" value="<?php print $add ?>" class="form-control" placeholder="Endereço" name="address" required >
                                                                </div>


                                                                <div class="form-group">
                                                                    <label>Email</label>
                                                                    <input type="text" value="<?php print $email ?>" class="form-control" placeholder="Email" name="email" required>
                                                                </div>


                                                                <div class="form-group">
                                                                    <label>Tipo de conta</label>
                                                                    <select name="acctype" required>
                                                                        <option value='0'>Selecione</option>	  
                                                                        <option value='1'>Corrente</option>
                                                                        <option value='2'>Poupança</option>

                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Nome do Banco</label>
                                                                    <input type="text" value="<?php print $bname ?>" class="form-control" placeholder="Nome do banco" name="bankname">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Nome do títular</label>
                                                                    <input type="text" disabled="" value="<?php print $accnamee ?>" class="form-control" placeholder="Títular" name="accname">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Conta</label>
                                                                    <input type="text" value="<?php print $accnumber ?>" class="form-control" placeholder="Conta" name="accno">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Agência</label>
                                                                    <input type="text" value="<?php print $ifsc ?>" class="form-control" placeholder="Agência" name="ifsccode">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Operação</label>
                                                                    <input type="text" value="<?php print $operacao ?>" class="form-control" placeholder="Operação" name="operacao">
                                                                </div>
                                                                <input type="hidden" value=""  name="sno">
                                                                <div class="form-group">
                                                                    <label>Senha (Caso deseje alterar)</label>
                                                                    <input type="password" value="" class="form-control" placeholder="Senha" name="p1" >
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Digite sua senha novamente</label>
                                                                    <input type="password" value="" class="form-control" placeholder="Repita sua senha" name="p2"  >
                                                                </div>




                                                        </div>

                                                        <button type="submit" class="btn btn btn-primary btn-block">Atualizar dados</button>
                                                        </form>
                                                    </div>

                                                </div>



                                            </div>
                                    </div>
                            </section>
                            </div>
                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section>
            </section>
        </section>
    </section>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/app.v1.js"></script>
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="js/app.plugin.js"></script>
</body>
</html>