<?php

include_once ("z_db.php");
$util->exibirErros();
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['username'])) {
    redirect('index.php');
}
$DB->where('id', $DB->escape($_GET['id']));
$DB->where('status', 1);
$DB->where('username', $_SESSION['username']);
$upda=$DB->update('pagamentos', array('status' => 55, 'user_operacao' => $_SESSION['username']));
if ($upda) {
    redirect('faturas.php?info=Operação realizada com sucesso!');
} else {
    redirect('faturas.php?info=Ocorreu um erro!');
}
?>