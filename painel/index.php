<?php
include_once("z_db.php");
$sql = "SELECT maintain FROM  settings WHERE sno=0";
if ($result = mysqli_query($con, $sql)) {

    /* fetch associative array */
    while ($row = mysqli_fetch_row($result)) {
        $main = $row[0];
    }
    if ($main == 1 || $main == 3) {
        print "
				<script language='javascript'>
					window.location = 'maintain.php';
				</script>
			";
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['username'])) {
    $status = "OK"; //initial status
    $msg = "";
    $username = mysqli_real_escape_string($con, $_POST['username']); //fetching details through post method
    $password = mysqli_real_escape_string($con, $_POST['password']);
    $site_key = site_key;
    $site_sec = site_secret;
    @$json = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$site_sec}&response=" . $_POST['g-recaptcha-response']);
    $res = json_decode($json);

    if ($res->success != 1 or $res->success == '') {
        $msg = $msg . "Captcha inválido<BR>";
        $status = "NOTOK";
    }
    if (strlen($username) < 6) {
        $msg = $msg . "Username must be more than 5 char legth<BR>";
        $status = "NOTOK";
    }

    if (strlen($password) < 6) { //checking if password is greater then 8 or not
        $msg = $msg . "Password must be more than 5 char legth<BR>";
        $status = "NOTOK";
    }

    if ($status == "OK") {


        $DB->where('username', $_POST['username']);
        $DB->where('level', 2);
        $DB->where('password', $_POST['password']);
        $userData = $DB->getOne('affiliateuser');
        if ($DB->count > 0) {
            if ($userData['active'] == 0 and $userData['renovacao_status'] == 1) {
                $_SESSION['renova_usr'] = $userData['username'];
                $_SESSION['paypalidsession'] = $userData['username'];
                redirect('renovar.php');
            } else if ($userData['active'] == 0) {
                $_SESSION['inativo_usr'] = $userData['username'];
                $_SESSION['paypalidsession'] = $userData['username'];
                redirect('inativo.php');
            }
        } else {
            $errormsg = "
<div class='alert alert-danger'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <i class='fa fa-ban-circle'></i><strong>Please Fix Below Errors : </br></strong>Dados não encontrados.</div>"; //printing error if found in validation

            $statusflag = "NOTOK";
        }


// Retrieve username and password from database according to user's input, preventing sql injection
        $query = "SELECT * FROM affiliateuser WHERE (username = '" . mysqli_real_escape_string($con, $_POST['username']) . "') AND (password = '" . mysqli_real_escape_string($con, $_POST['password']) . "') AND (active = '" . mysqli_real_escape_string($con, "1") . "') AND (level = '" . mysqli_real_escape_string($con, "2") . "')";
        if ($stmt = mysqli_prepare($con, $query)) {

            /* execute query */
            mysqli_stmt_execute($stmt);

            /* store result */
            mysqli_stmt_store_result($stmt);

            $num = mysqli_stmt_num_rows($stmt);

            /* close statement */
            mysqli_stmt_close($stmt);
        }
//mysqli_close($con);
// Check username and password match
        if (($num) == 1) {




            $sqlquery11 = "SELECT expiry FROM affiliateuser where username = '$username'"; //fetching expiry date of username from table
            $rec211 = mysqli_query($con, $sqlquery11);
            $row211 = mysqli_fetch_row($rec211);
            $expirydate = $row211[0]; //assigning expiry date

            $curdate = date("Y-m-d");
            if ($curdate > $expirydate) {
                $errormsg = "
<div class='alert alert-danger'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <i class='fa fa-ban-circle'></i><strong>Please Fix Below Errors : </br></strong>Hello User, Your Account Has Been Deactivated, As Your Account Is Expired. Please Check Below To Renew Your Account.</div>"; //printing error if found in validation

                $statusflag = "NOTOK";
            } else {

                // Set username session variable
                $_SESSION['username'] = $username;

                // Jump to secured page
                print "
				<script language='javascript'>
					window.location = 'dashboard.php?page=dashboard%location=index.php';
				</script>";
            }
        } else {
            $errormsg = "
<div class='alert alert-danger'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
					<i class='fa fa-ban-circle'></i><strong>Ops, ocorreu um erro: </br></strong>- Login ou senha incorretos, ou<br>- Esse usuário está inativo</div>"; //printing error if found in validation
        }
    } else {

        $errormsg = "
<div class='alert alert-danger'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <i class='fa fa-ban-circle'></i><strong>Ops, ocorreu um erro: </br></strong>- $msg <br>- Login ou senha incorretos, ou<br>- Esse usuário está inativo</div>"; //printing error if found in validation
    }
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8" />
        <title><?= DESC_DEF ?></title>
        <meta name="description" content="Sistema para Marketing Multinível" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <link rel="icon" href="images/favicon.png" type="favicon" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
        <style type="text/css">html {
                overflow-y: scroll;
                background: url(images/background.png) !important no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
        </style>       
    </head>
    <body style="background-image: url('http://www.100resilientcities.org/page/-/100rc/img/cities/cities-nyc_optimized.jpg')">
        <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
            <div class="container aside-xl">
                <section class="m-b-lg">
                    <br>
                    <a href="index.php"><img src="images/login.png" align="center" style="max-width:100%; padding-bottom:5px"></a>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"], ENT_QUOTES, "utf-8"); ?>" method="post">
                        <div class="list-group">
                            <?php
                            if ($_SERVER['REQUEST_METHOD'] == 'POST' && ($errormsg != "")) {
                                print $errormsg;
                            }
                            ?>
                            <div class="list-group-item">
                                <input type="text" placeholder="Usuário" class="form-control no-border" name="username" required>
                            </div>
                            <div class="list-group-item">
                                <input type="password" placeholder="Senha" class="form-control no-border" name="password" required>
                            </div>
                            <br>
                            <div class="g-recaptcha" data-sitekey="<?= site_key ?>"></div>
                        </div>

                        <br>
                        <button type="submit" class="btn btn-lg btn-primary btn-block">Acessar</button>
                        <div class="text-center m-t m-b"><a href="forgotpassword.php"><small style="color:#fff;">Esqueceu sua senha?</small></a>
                            <div class="line line-dashed"></div>
                    </form>
                </section>
            </div>
        </section>
        <!-- footer -->
        <footer id="footer">
            <div class="text-center padder">
                <p> <small style="color:#000000;"><?php
                        $query = "SELECT footer from settings where sno=0";


                        $result = mysqli_query($con, $query);

                        while ($row = mysqli_fetch_array($result)) {
                            $footer = "$row[footer]";
                            print $footer;
                        }
                        ?></small> </p>
            </div>
        </footer>
        <!-- / footer -->
        <!-- Bootstrap -->
        <!-- App -->
        <script src="js/app.v1.js"></script>
        <script src="js/app.plugin.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </body>
</html>