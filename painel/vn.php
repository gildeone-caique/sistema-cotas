<?php

include_once ("z_db.php");
$configData = $DB->getOne('settings');
$today = date('y-m-d');


// Inialize session
set_time_limit(180);
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['username'])) {
    redirect('index.php');
}
$DB->where('id', $DB->escape($_GET['id']));
$DB->where('user_id', $_SESSION['username']);
$res = $DB->update('notifications', array('status' => 1));
redirect('dashboard.php');
?>