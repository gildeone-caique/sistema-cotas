<?php
include_once ("z_db.php");
// Inialize session
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    extract($_POST);
    $status = "OK";
    $msg = "";
    if ($status == '') {
        $msg = $msg . "Insira um status.<BR>";
        $status = "NOTOK";
    }
    if ($nome == '') {
        $msg = $msg . "Insira uma nome.<BR>";
        $status = "NOTOK";
    } if ($descricao == '') {
        $msg = $msg . "Insira uma descrição.<BR>";
        $status = "NOTOK";
    }
    if ($tipo == 1) {
        if ($link == '') {
            $msg = $msg . "Insira um link.<BR>";
            $status = "NOTOK";
        }
    } elseif ($tipo == 2) {
        if ($link_youtube == '') {
            $msg = $msg . "Insira um link para o youtube.<BR>";
            $status = "NOTOK";
        }
    } elseif ($tipo == 3) {
        if ($_FILES['upload'] == '') {
            $msg = $msg . "Insira um arquivo.<BR>";
            $status = "NOTOK";
        }
    } else {
        $msg = $msg . "Selecione um formato.<BR>";

        $status = "NOTOK";
    }
    if ($tipo == 3) {
        define('SITE_ROOT3', str_replace('admin', '', realpath(dirname(__FILE__))));
        /**
         * Upload de Imagens com Segurança
         *
         * @author Alfred Reinold Baudisch
         * @email alfred_baudisch@hotmail.com
         * @date Jan 09, 2004
         * @changes Jan 14, 2004 - v2.0
         */
// Prepara a variável caso o formulário tenha sido postado
        $arquivo = isset($_FILES["upload"]) ? $_FILES["upload"] : FALSE;

        $config = array();
// Tamano máximo da imagem, em bytes
        $config["tamanho"] = 10060883;
// Largura Máxima, em pixels
        $config["largura"] = 5550;
// Altura Máxima, em pixels
        $config["altura"] = 5550;
// Diretório onde a imagem será salva
        $config["diretorio"] = SITE_ROOT3 . "/downloads/";

// Gera um nome para a imagem e verifica se já não existe, caso exista, gera outro nome e assim sucessivamente..
// Função Recursiva
        function nome($extensao) {
            global $config;

// Gera um nome único para a imagem
            $temp = substr(md5(uniqid(time())), 0, 10);
            $imagem_nome = $temp . "." . $extensao;

// Verifica se o arquivo já existe, caso positivo, chama essa função novamente
            if (file_exists($config["diretorio"] . $imagem_nome)) {
                $imagem_nome = nome($extensao);
            }

            return $imagem_nome;
        }

        if ($arquivo) {
            $erro = array();
            if ($arquivo['type'] == 'application/x-php') {
                $msg = $msg . "Formato inválido.<BR>";
                $status = "NOTOK";
            } else if ($arquivo["size"] > $config["tamanho"]) {
                $msg = $msg . "Arquivo em tamanho muito grande!A imagem deve ser de no máximo " . $config["tamanho"] . " bytes. Envie outro arquivo<BR>";
                $status = "NOTOK";
            } else {
                $arquivoInfo = pathinfo($arquivo['name']);
                $nameFile = $arquivoInfo['filename'] . '-' . md5(time()) . '.' . $arquivoInfo['extension'];
                $arquivo_dir = $config["diretorio"] . $nameFile;
                if (move_uploaded_file($arquivo["tmp_name"], $arquivo_dir)) {
                    
                } else {
                    $msg = $msg . "Ocorreu um erro ao salvar o arquivo";
                    $status = "NOTOK";
                }
            }
        }

        //$DB->insert('materiais', ['nome' => $nome, 'tipo' => $tipo, 'link' => $link_youtube, 'status' => $status, 'descricao' => $descricao]);
    }
    if ($status == "OK") {
        $status = $_POST['status'];

        if ($tipo == 1) {
            $DB->insert('materiais', ['nome' => $nome, 'tipo' => $tipo, 'link' => $link, 'status' => $status, 'descricao' => $descricao]);
        }
        if ($tipo == 2) {
            $DB->insert('materiais', ['nome' => $nome, 'tipo' => $tipo, 'link' => $link_youtube, 'status' => $status, 'descricao' => $descricao]);
        }
        if ($tipo == 3) {
            $DB->insert('materiais', ['nome' => $nome, 'tipo' => $tipo, 'link' => $nameFile, 'status' => $status, 'descricao' => $descricao]);
        }



        $errormsg = "
<div class='alert alert-success'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <i class='fa fa-ban-circle'></i><strong>Success : </br></strong>Your profile has been updated.</div>"; //printing error if found in validation
    } else {
        $errormsg = "
<div class='alert alert-danger'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <i class='fa fa-ban-circle'></i><strong>Please Fix Below Errors : </br></strong>" . $msg . "</div>"; //printing error if found in validation
    }
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8" />
        <title><?= TITULO_DEF ?></title>
        <meta name="description" content="Sistema para Marketing Multinível" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
        <link rel="icon" href="images/favicon.png" type="favicon" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
    </head>
    <body class="">
        <section class="vbox">
            <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
                <div class="navbar-header aside-md dk"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="fa fa-bars"></i> </a> <a href="dashboard.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm"> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i> </a> </div>


                <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">

                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="images/a0.jpg"> </span> <?php
                            $sql = "SELECT fname FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                            if ($result = mysqli_query($con, $sql)) {

                                /* fetch associative array */
                                while ($row = mysqli_fetch_row($result)) {
                                    print $row[0];
                                }
                            }
                            ?><b class="caret"></b> </a>
                        <ul class="dropdown-menu animated fadeInRight">
                            <span class="arrow top"></span>
                            <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
                        </ul>
                    </li>
                </ul>
            </header>
            <section>
                <section class="hbox stretch">
                    <!-- .aside -->
                    <aside class="bg-light aside-md hidden-print" id="nav">
                        <section class="vbox">
                            <section class="w-f scrollable">
                                <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-color="#333333">
                                    <div class="clearfix wrapper dk nav-user hidden-xs">
                                        <div class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb avatar pull-left m-r"> <img src="images/a0.jpg"> <i class="on md b-black"></i> </span> <span class="hidden-nav-xs clear"> <span class="block m-t-xs"> <strong class="font-bold text-lt"><?php
                                                            $sql = "SELECT fname,country FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                                                            if ($result = mysqli_query($con, $sql)) {

                                                                /* fetch associative array */
                                                                while ($row = mysqli_fetch_row($result)) {
                                                                    print $row[0];
                                                                    $coun = $row[1];
                                                                }
                                                            }
                                                            ?></strong> <b class="caret"></b> </span> <span class="text-muted text-xs block">Administrador</span> </span> </a>
                                        </div>
                                    </div>
                                    <!-- nav -->
                                    <nav class="nav-primary hidden-xs">
                                        <?php
                                        include('menu.php');
                                        ?>
                                    </nav>
                                    <!-- / nav -->
                                </div>
                            </section>
                            <footer class="footer hidden-xs no-padder text-center-nav-xs"> <a href="logout.php" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs"> <i class="i i-logout"></i> </a> <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs"> <i class="i i-circleleft text"></i> <i class="i i-circleright text-active"></i> </a> </footer>
                        </section>
                    </aside>
                    <!-- /.aside -->
                    <section id="content">
                        <section class="vbox">
                            <section class="scrollable wrapper">
                                <div class="row">

                                    <div class="col-sm-12 portlet">
                                        <section class="panel panel-success portlet-item">
                                            <header class="panel-heading"> Criar Material </header>
                                            <section class="panel panel-default">
                                                <header class="panel-heading bg-light">
                                                    <?= @$errormsg ?>
                                                </header>
                                                <div class="panel-body">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="home">


                                                            <div class="panel-body">
                                                                <form enctype='multipart/form-data' method="post">
                                                                    <div class="form-group">
                                                                        <label>Nome do pacote</label>
                                                                        <input type="text" class="form-control" placeholder="Nome do pacote" name="nome" required="">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Descrição</label>
                                                                        <input type="text" class="form-control" placeholder="Ex: Material  de motivação" name="descricao" required="">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Formato</label>
                                                                        <select name="tipo" id="tipo" class="form-control" required="">
                                                                            <option value="">Selecione</option>
                                                                            <option value="1">Link</option>
                                                                            <option value="2">Link de video do youtube</option>
                                                                            <option value="3">Upload de arquivo</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group" id="link" style="display: none;">
                                                                        <label>Link</label>
                                                                        <input type="url" class="form-control" placeholder="Ex: http://google.com ou http://site.com/arquivo.pdf" name="link">
                                                                    </div>
                                                                    <div class="form-group" id="link_youtube" style="display: none;">
                                                                        <label>Video do youtube</label>
                                                                        <input type="url" class="form-control" placeholder="Ex: https://www.youtube.com/watch?v=2qqN4cEpPCw" name="link_youtube">
                                                                    </div>
                                                                    <div class="form-group" id="upload" style="display: none;">
                                                                        <label>Arquivo</label>
                                                                        <input type="file" class="form-control" placeholder="Ex: https://www.youtube.com/watch?v=2qqN4cEpPCw" name="upload">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Publicar</label>
                                                                        <select name="status" class="form-control" required="">
                                                                            <option value="">Selecione</option>
                                                                            <option value="1">Sim</option>
                                                                            <option value="2">Não</option>
                                                                        </select>
                                                                    </div>


                                                            </div>
                                                        </div>

                                                    </div>

                                                    <button type="submit" class="btn btn-sm btn-default" style="background-color:#ffd800; border-color:#ffd800;"> <font color="#ffffff"><b>Criar</b> </font></button>

                                                </div>
                                                </form>
                                                </div>

                                                </div>
                                                </div>
                                            </section>
                                        </section>

                                    </div>
                                </div>
                            </section>
                        </section>
                        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section>
                </section>
            </section>
        </section>
        <!-- Bootstrap -->
        <!-- App -->
        <script src="js/app.v1.js"></script>
        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="js/jquery.ui.touch-punch.min.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="js/app.plugin.js"></script>
        <script>
            $("#tipo").change(function () {
                var valor = $(this).val();
                if (valor == 1) {
                    $("#link_youtube").hide();
                    $("#upload").hide();
                    $("#link").show();
                }
                if (valor == 2) {
                    $("#link_youtube").show();
                    $("#upload").hide();
                    $("#link").hide();
                }
                if (valor == 3) {
                    $("#link_youtube").hide();
                    $("#upload").show();
                    $("#link").hide();
                }


            });
        </script>
    </body>
</html>