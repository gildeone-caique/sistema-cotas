<?php
include_once ("z_db.php");
// Inialize session
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}
$toupdate = mysqli_real_escape_string($con, $_GET['username']);
?>
<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8" />
        <title><?= TITULO_DEF ?></title>
        <meta name="description" content="Sistema para Marketing Multinível" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
        <link rel="icon" href="images/favicon.png" type="favicon" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
    </head>
    <body class="">
        <section class="vbox">
            <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
                <div class="navbar-header aside-md dk"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="fa fa-bars"></i> </a> <a href="dashboard.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm"> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i> </a> </div>


                <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">

                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="images/a0.jpg"> </span> <?php
                            $sql = "SELECT fname FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                            if ($result = mysqli_query($con, $sql)) {

                                /* fetch associative array */
                                while ($row = mysqli_fetch_row($result)) {
                                    print $row[0];
                                }
                            }
                            ?><b class="caret"></b> </a>
                        <ul class="dropdown-menu animated fadeInRight">
                            <span class="arrow top"></span>
                            <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
                        </ul>
                    </li>
                </ul>
            </header>
            <section>
                <section class="hbox stretch">
                    <!-- .aside -->
                    <aside class="bg-black aside-md hidden-print" id="nav">
                        <section class="vbox">
                            <section class="w-f scrollable">
                                <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-color="#333333">
                                    <div class="clearfix wrapper dk nav-user hidden-xs">
                                        <div class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb avatar pull-left m-r"> <img src="images/a0.jpg"> <i class="on md b-black"></i> </span> <span class="hidden-nav-xs clear"> <span class="block m-t-xs"> <strong class="font-bold text-lt"><?php
                                                            $sql = "SELECT fname,country FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                                                            if ($result = mysqli_query($con, $sql)) {

                                                                /* fetch associative array */
                                                                while ($row = mysqli_fetch_row($result)) {
                                                                    print $row[0];
                                                                    $coun = $row[1];
                                                                }
                                                            }
                                                            ?></strong> <b class="caret"></b> </span> <span class="text-muted text-xs block">Administrador</span> </span> </a>
                                        </div>
                                    </div>
                                    <!-- nav -->
                                    <nav class="nav-primary hidden-xs">
                                        <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">MENU DE ADMINISTRAÇÃO</div>
                                        <ul class="nav nav-main" data-ride="collapse">
                                            <li class="active" > <a href="dashboard.php" class="auto"><i class="i i-statistics icon"></i> <span>Visão Geral</span> </a> </li>
                                            <li> <a href="gensettings.php" class="auto"> <i class="fa fa-cog" aria-hidden="true"></i> <span>Configurações Gerais</span> </a> </li>
                                            <li> <a href="emailsettings.php" class="auto"> <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Gerenciar e-mails</span> </a> </li>
                                            <li> <a href="pacsettings.php" class="auto"> <i class="fa fa-tasks" aria-hidden="true"></i> <span>Gerenciar Planos</span> </a> </li>
                                            <li> <a href="notifications.php" class="auto"><i class="fa fa-tags" aria-hidden="true"></i> <span>Notificações</span> </a> </li>
                                            <li> <a href="users.php" class="auto"><i class="fa fa-user"></i> <span>Usuários</span> </a> </li>
                                            <li> <a href="payments.php" class="auto"><i class="fa fa-money" aria-hidden="true"></i> <span>Pagamentos via Paypal</span> </a> </li>
                                            <li> <a href="paymentscod.php" class="auto"><i class="fa fa-exchange" aria-hidden="true"></i> <span>Pagamentos em dinheiro </span> </a> </li>
                                            <li> <a href="payrequest.php" class="auto"><i class="fa fa-refresh" aria-hidden="true"></i> <span>Requisições de Pagamento </span> </a> </li>
                                            <li> <a href="renewpaymentscod.php" class="auto"><i class="fa fa-plus" aria-hidden="true"></i> <span>Renovações </span> </a> </li>

                                        </ul>
                                        <div class="line dk hidden-nav-xs"></div>


                                    </nav>	
                                    <!-- / nav -->
                                </div>
                            </section>
                            <footer class="footer hidden-xs no-padder text-center-nav-xs"> <a href="logout.php" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs"> <i class="i i-logout"></i> </a> <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs"> <i class="i i-circleleft text"></i> <i class="i i-circleright text-active"></i> </a> </footer>
                        </section>
                    </aside>
                    <!-- /.aside -->
                    <section id="content">
                        <section class="vbox">
                            <section class="scrollable wrapper">
                                <div class="row">

                                    <div class="col-sm-12 portlet">
                                        <section class="panel panel-success portlet-item">
                                            <header class="panel-heading"> Editar Usuário |  <a href='deleteuser.php?username=$username'>Remover Usuário</a> </header>
                                            <section class="panel panel-default">
                                                <ul class="nav nav-tabs nav-justified">
                                                    <?php
                                                    $query = "SELECT * FROM  affiliateuser where username='$toupdate' ";


                                                    $result = mysqli_query($con, $query);
                                                    $i = 0;
                                                    while ($row = mysqli_fetch_array($result)) {

                                                        $id = "$row[Id]";
                                                        $username = "$row[username]";
                                                        $pass = "$row[password]";
                                                        $address = "$row[address]";
                                                        $fname = "$row[fname]";
                                                        $email = "$row[email]";
                                                        $mobile = "$row[mobile]";
                                                        $active = "$row[active]";
                                                        $doj = "$row[doj]";
                                                        $country = "$row[country]";
                                                        $ear = "$row[tamount]";
                                                        $ref = "$row[referedby]";
                                                        $pck = "$row[pcktaken]";
                                                        $lprofile = "$row[launch]";
                                                        $cpf = "$row[cpf]";
                                                        if ($active == 1) {
                                                            $status = "Active/Paid";
                                                        } else if ($active == 0) {
                                                            $status = "UnActive/Unpaid";
                                                        } else {
                                                            $status = "Unknown";
                                                        }
                                                        extract($row);

                                                        $qu = "SELECT * FROM  packages where id = $pck";


                                                        $re = mysqli_query($con, $qu);

                                                        while ($r = mysqli_fetch_array($re)) {
                                                            $pckid = "$r[id]";
                                                            $pckname = "$r[name]";
                                                            $pckprice = "$r[price]";
                                                            $pcktax = "$r[tax]";
                                                            $pckcur = "$r[currency]";
                                                            $pcksbonus = "$r[sbonus]";
                                                        }
                                                        $total = $pckprice + $pcktax;
                                                    }
                                                    ?>  
                                                </ul>
                                                <div class="panel-body">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="home">


                                                            <div class="panel-body">
                                                                <form action="updateusersettings.php" method="post">
                                                                    <input type="hidden" value="ID" name="pckmainid">
                                                                    <div class="form-group">
                                                                        <label>Qual o status do usuário? 1 - Ativo / 0 - Inativo</label>
                                                                        <input type="text" value="<?php print $active ?>" class="form-control" placeholder="Enter 0 or 1" name="act">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Usuário</label>
                                                                        <input type="text" value="<?php print $toupdate ?>" class="form-control" placeholder="Login" name="us" >
                                                                    </div>


                                                                    <input type="hidden" value="<?php print $toupdate ?>" class="form-control" placeholder="Login" name="username">

                                                                    <div class="form-group">
                                                                        <label>Nome Completo</label>
                                                                        <input type="text" value="<?php print $fname ?>" class="form-control" placeholder="Nome Completo" name="fname">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Cpf</label>
                                                                        <input type="text" value="<?php print $cpf; ?>" class="form-control" placeholder="Nome Completo" name="fname">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Estado</label>
                                                                        <input type="text" value="<?php print $estado ?>" maxlength="2" class="form-control" placeholder="Estado.Ex: BA" name="estado" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Cidade</label>
                                                                        <input type="text" value="<?php print $cidade ?>" class="form-control" placeholder="Cidade" name="cidade" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Endereço</label>
                                                                        <input type="textarea" value="<?php print $address ?>" class="form-control" placeholder="Address" name="address">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Senha (Caso deseje alterar)</label>
                                                                        <input type="textarea" value="<?php print $pass ?>" class="form-control" placeholder="Senha" name="password">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>Cod</label>
                                                                        <input type="text" value="<?php print $cod ?>" class="form-control" placeholder="Cidade" name="cod" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Email</label>
                                                                        <input type="text" value="<?php print $email ?>" class="form-control" placeholder="Email" name="email" >
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>Telefone</label>
                                                                        <input type="text" value="<?php print $mobile ?>" class="form-control" placeholder="Telefone" name="mobile" >
                                                                    </div>



                                                                    <div class="form-group">
                                                                        <label>Saldo</label>
                                                                        <input type="text" value="<?php print $ear ?>" class="form-control" placeholder="Saldo" name="earnings" >
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>Patrocinador</label>
                                                                        <input type="text" value="<?php print $ref ?>" class="form-control" placeholder="Patrocinador" name="refer" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Plano</label>
                                                                        <input type="text" value="<?php print $pckname ?>" class="form-control" placeholder="Referred By" name="pck" disabled >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>
                                                                            Deseja fazer upgrade desse usuário? 
                                                                            <select name="package">
                                                                                <?php
                                                                                $query = "SELECT id,name,price,currency,tax FROM  packages where active=1";


                                                                                $result = mysqli_query($con, $query);

                                                                                while ($row = mysqli_fetch_array($result)) {
                                                                                    $id = "$row[id]";
                                                                                    $pname = "$row[name]";
                                                                                    $pprice = "$row[price]";
                                                                                    $pcur = "$row[currency]";
                                                                                    $ptax = "$row[tax]";
                                                                                    $total = $pprice + $ptax;
                                                                                    print "<option value='$id'>$pname | Price - $pcur $total </option>";
                                                                                }
                                                                                ?>

                                                                            </select>




                                                                    </div>







                                                            </div>

                                                            <button type="submit" class="btn btn-lg btn-primary btn-block">Salvar alterações</button>
                                                            </form>
                                                        </div>

                                                    </div>



                                                </div>
                                                </div>
                                            </section>
                                        </section>

                                    </div>
                                </div>
                            </section>
                        </section>
                        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section>
                </section>
            </section>
        </section>
        <!-- Bootstrap -->
        <!-- App -->
        <script src="js/app.v1.js"></script>
        <script src="js/jquery.ui.touch-punch.min.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="js/app.plugin.js"></script>
    </body>
</html>