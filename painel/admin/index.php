<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8" />
        <title>Administração</title>
        <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
        <style type="text/css">html {
                overflow-y: scroll;
                background: url(images/login2.jpg) no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }

        </style>
    <div id="google_translate_element" align="right"></div><script type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
        }
    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

</head>
<body style="background-image: url('http://www.100resilientcities.org/page/-/100rc/img/cities/cities-nyc_optimized.jpg')">
    <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
        <div class="container aside-xl">
            <section class="m-b-lg>
                     <br>
                     <a href="index.php"><img src="images/login.png" align="center" style="max-width:100%; padding-bottom:5px"></a>
                <form action="loginproc.php" method="post" >
                    <div class="list-group">
                        <div class="list-group-item">
                            <input type="text" placeholder="Usuário" class="form-control no-border" name="username" required>
                        </div>
                        <div class="list-group-item">
                            <input type="password" placeholder="Senha" class="form-control no-border" name="password" required>
                        </div>
                        <br>
                        <div class="g-recaptcha" data-sitekey="<?= site_key ?>"></div>

                    </div>
                    <button type="submit" class="btn btn-lg btn-primary btn-block">Acessar</button>
                    <div class="line line-dashed"></div>

                </form>
            </section>
        </div>
    </section>
    <!-- footer -->
    <footer id="footer">
    </footer>
    <!-- / footer -->
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/app.v1.js"></script>
    <script src="js/app.plugin.js"></script>
    <script>
<?php
if (isset($_GET['maintain'])) {

    echo "alert(" . $_GET['maintain'] . ")";
}
?>
    </script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

</body>
</html>