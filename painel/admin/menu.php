<ul class="nav nav-main" data-ride="collapse">
    <li class="active" > <a href="dashboard.php" class="auto"><i class="i i-statistics icon"></i> <span>Visão Geral</span> </a> </li>
    <li> <a href="gensettings.php" class="auto"> <i class="fa fa-cog" aria-hidden="true"></i> <span>Configurações Gerais</span> </a> </li>
    <li> <a href="emailsettings.php" class="auto"> <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Gerenciar e-mails</span> </a> </li>
    <li> <a href="pacsettings.php" class="auto"> <i class="fa fa-tasks" aria-hidden="true"></i> <span>Gerenciar Planos</span> </a> </li>
    <li> <a href="notifications.php" class="auto"><i class="fa fa-tags" aria-hidden="true"></i> <span>Notificações</span> </a> </li>
    <li> <a href="users.php" class="auto"><i class="fa fa-user"></i> <span>Usuários</span> </a> </li>
    <li> <a href="faturas.php" class="auto"><i class="fa fa-exchange" aria-hidden="true"></i> <span>Pagamentos</span> </a> </li>
    <li> <a href="payrequest.php" class="auto"><i class="fa fa-refresh" aria-hidden="true"></i> <span>Requisições de Pagamento </span> </a> </li>
    <li> <a href="renewpaymentscod.php" class="auto"><i class="fa fa-plus" aria-hidden="true"></i> <span>Renovações </span> </a> </li>
    <li> <a href="extratos.php" class="auto"><i class="fa fa-money" aria-hidden="true"></i> <span>Extratos </span> </a> </li>
    <li> <a data-toggle="modal" data-target="#myModal" class="auto"><i class="fa fa-scissors" aria-hidden="true"></i> <span>Divisão de lucro </span> </a> </li>
    <li> <a href="listarMateriais.php" class="auto"><i class="fa fa-book" aria-hidden="true"></i> <span>Materiais </span> </a> </li>
    <li> <a href="criarMaterial.php" class="auto"><i class="fa fa-book" aria-hidden="true"></i> <span>Criar Material </span> </a> </li>

</ul>
<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <form action="dashboard.php" method="post" onsubmit="if (!confirm('Tem certeza?')) {
                return false;
            }" >
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Divisão de lucros</h4>
                </div>
                <div class="modal-body">
                    <p>
                    <div class="form-group">
                        <label>Valor </label>
                        <input class="form-control" type="number" min="1" name="valorDist" id="valorDist" required="">
                        <input class="form-control" type="hidden" min="1" name="todo" value="divisao" >

                        <p>

                    </div>   
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" id="distribuir" class="btn btn-primary">Distribuir</button>
                </div>

            </div>

        </div>
    </form>
</div>
