<?php
include_once ("z_db.php");
// Inialize session
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}
?>
<?php
$util->faturasVencidasDel();
$query = "SELECT id,fname,email,doj,active,username,address,pcktaken,expiry FROM  affiliateuser where username = '" . $_SESSION['adminidusername'] . "'";


$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result)) {
    $aid = "$row[id]";
    $regdate = "$row[doj]";
    $name = "$row[fname]";
    $address = "$row[address]";
    $acti = "$row[active]";
    $pck = "$row[pcktaken]";
    $regexpiry = "$row[expiry]";
}
?>

<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8" />
        <title><?= TITULO_DEF ?></title>
        <meta name="description" content="Sistema para Marketing Multinível" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" type="text/css" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->

    </head>
    <body class="">
        <section class="vbox">
            <header class="bg-primary header header-md navbar navbar-fixed-top-xs box-shadow">
                <div class="navbar-header aside-md dk"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="fa fa-bars"></i> </a> <a href="dashboard.php" class="navbar-brand"><img src="images/logo.png" style="max-height:50px !important"><?php
                        $query = "SELECT header from settings where sno=0";


                        $result = mysqli_query($con, $query);

                        while ($row = mysqli_fetch_array($result)) {
                            $header = "$row[header]";
                            print $header;
                        }
                        ?></a> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i> </a> </div>


                <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">

                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="images/a0.png"> </span> <?php
                            $sql = "SELECT fname FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                            if ($result = mysqli_query($con, $sql)) {

                                /* fetch associative array */
                                while ($row = mysqli_fetch_row($result)) {
                                    print $row[0];
                                }
                            }
                            ?> <b class="caret"></b> </a>
                        <ul class="dropdown-menu animated fadeInRight">
                            <span class="arrow top"></span>
                            <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
                        </ul>
                    </li>
                </ul>
            </header>
            <section>
                <section class="hbox stretch">
                    <!-- .aside -->
                    <aside class="bg-light aside-md hidden-print" id="nav">
                        <section class="vbox">
                            <section class="w-f scrollable">
                                <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-color="#333333">
                                    <div class="clearfix wrapper dk nav-user hidden-xs">
                                        <div class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb avatar pull-left m-r"> <img src="images/a0.jpg"> <i class="on md b-black"></i> </span> <span class="hidden-nav-xs clear"> <span class="block m-t-xs"> <strong class="font-bold text-lt"><?php
                                                            $sql = "SELECT fname,country,pcktaken FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                                                            if ($result = mysqli_query($con, $sql)) {

                                                                /* fetch associative array */
                                                                while ($row = mysqli_fetch_row($result)) {
                                                                    print $row[0];
                                                                    $coun = $row[1];
                                                                    $pcktaken = $row[2];
                                                                    $sql2 = "SELECT name FROM packages WHERE id=$pcktaken";
                                                                    if ($result2 = mysqli_query($con, $sql2)) {
                                                                        while ($row2 = mysqli_fetch_row($result2)) {

                                                                            $pkname = $row2[0];
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            ?></strong> <b class="caret"></b> </span> <span class="text-muted text-xs block"><?php print "$pkname Member"; ?></span> </span> </a>
                                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                                <span class="arrow top hidden-nav-xs"></span>
                                                <li> <a href="profile.php">Perfil</a> </li>
                                                <li> <a href="notifications.php"> Notificações</a> </li>
                                                <li> <a href="contact.php">Suporte</a> </li>
                                                <li class="divider"></li>
                                                <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- nav -->
                                    <nav class="nav-primary hidden-xs">
                                        <?php
                                        include('menu.php');
                                        ?>
                                    </nav>
                                    <!-- / nav -->
                                </div>
                            </section>
                            <footer class="footer hidden-xs no-padder text-center-nav-xs"> <a href="logout.php" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs"> <i class="i i-logout"></i> </a> <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs"> <i class="i i-circleleft text"></i> <i class="i i-circleright text-active"></i> </a> </footer>
                        </section>
                    </aside>
                    <!-- /.aside -->
                    <section id="content">
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="home">


                                    <div class="panel-body">


                                        <div class="table-responsive">
                                            <h1>Faturas</h1>
                                            <table class="table table-striped b-t b-light">
                                                <thead>
                                                    <tr>

                                                        <th width="5%">ID</th>
                                                        <th width="10%">Usuário</th>
                                                        <th width="10%">Valor</th>
                                                        <th width="10%">Pagamentp</th>

                                                        <th width="10%">Data</th>
                                                        <th width="10%">Vencimento</th>
                                                        <th width="10%">Valor</th>
                                                        <th width="15%">Descrição</th>
                                                        <th width="10%">Link</th>
                                                        <th width="10%">Comprovante</th>
                                                        <th width="10%">Ações</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $usrNm = $_SESSION['adminidusername'];
                                                    $query = "SELECT * FROM  pagamentos ORDER BY id DESC";


                                                    $result = mysqli_query($con, $query);
                                                    $i = 0;
                                                    while ($row = mysqli_fetch_array($result)) {

                                                        $id = "$row[id]";
                                                        $usuario = "$row[username]";
                                                        $valor = "$row[valor]";
                                                        $dt = "$row[data_criacao]";
                                                        $descricacao = "$row[descricao]";
                                                        $vencimento = "$row[vencimento]";
                                                        $comprovate = "$row[comprovante]";
                                                        $status = $util->statusPagamento("$row[status]");
                                                        $user_act = "$row[user_operacao]";

                                                        if ($status == 'Pago') {
                                                            $row['url'] = 'javascript:alert("Essa fatura já foi paga");';
                                                        }
                                                        if ($status == 'Cancelada') {
                                                            $row['url'] = 'javascript:alert("Essa fatura já foi cancelada");';
                                                        }
                                                        $url = "$row[url]";
                                                        if ($comprovate != '') {
                                                            $comprovante = URL . '/comprovantes/' . $comprovate;
                                                            $comprovante = "<a href='verComprovante.php?id=$id' onclick='return ctz()'>Ver comprovante</a>";
                                                        } else {
                                                            $comprovante = 'javascript:alert("Não enviado");';
                                                            $comprovante = "<a href='$comprovante'>Comprovante não enviado</a>";
                                                        }
                                                        if ($status == 'Cancelada') {
                                                            $status = 'Cancelada por ' . $user_act;
                                                            $linkOP = " ";
                                                            $form = "";
                                                        }
                                                        $valor = number_format($valor, 2);

                                                        print "<tr>
				  
				  <td>
				  $id
				  </td>
				  <td>
				  $usuario
				  </td>
                                    <td>
				  R$ $valor
				  </td>
                                  <td>
				  $status
				  </td>
				  <td>
				   $dt 
				  </td>
                                  <td>
				   $vencimento 
				  </td>
                                  <td>
                                  $valor
                                  </td>
                                   <td>
                                  $descricacao
                                  </td>
				  <td>
                                  <a href='$url'>Clique Aqui para pagar</a>
                                  </td>
                                   <td>
                                  $comprovante
                                  </td>
                                  <td>
                       <a href='excluirFatura.php?id=$id' onclick='return ctz()'>Cancelar</a>||<a href='liberarFatura.php?id=$id' onclick='return ctz()'>Liberar</a>             
</td>
				

				  
				  </tr>";
                                                    }
                                                    ?>

                                                </tbody>
                                            </table>
                                        </div>




                                    </div>

                                </div>



                            </div>
                        </div>
                        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section>
                </section>
            </section>
        </section>
        <!-- Bootstrap -->
        <!-- App -->
        <script src="js/app.v1.js"></script>
        <script src="js/app.plugin.js"></script>
        <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script>
            function ctz() {

                if (!confirm('Você tem certeza?')) {
                    return false;
                }
            }
            $(document).ready(function () {
                $('table').DataTable();
            });
<?php
if (isset($_GET['info'])) {
    echo 'alert("' . $_GET['info'] . '");';
}
?>
        </script>
    </body>
</html>
