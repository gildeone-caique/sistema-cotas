<?php
include_once ("z_db.php");

// Inialize session
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8" />
        <title><?= TITULO_DEF ?></title>
        <meta name="description" content="Sistema para Marketing Multinível" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
        <link rel="icon" href="images/favicon.png" type="favicon" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
        <style>

            .bg-white{

            }
        </style>
    </head>
    <body class="">
        <section class="vbox">
            <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
                <div class="navbar-header aside-md dk"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="fa fa-bars"></i> </a> <a href="dashboard.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm"> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i> </a> </div>


                <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">

                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="images/a0.jpg"> </span> <?php
                            $sql = "SELECT fname FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                            if ($result = mysqli_query($con, $sql)) {

                                /* fetch associative array */
                                while ($row = mysqli_fetch_row($result)) {
                                    print $row[0];
                                }
                            }
                            ?><b class="caret"></b> </a>
                        <ul class="dropdown-menu animated fadeInRight">
                            <span class="arrow top"></span>
                            <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
                        </ul>
                    </li>
                </ul>
            </header>
            <section>
                <section class="hbox stretch">
                    <!-- .aside -->
                    <aside class="bg-light aside-md hidden-print" id="nav">
                        <section class="vbox">
                            <section class="w-f scrollable">
                                <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-color="#333333">
                                    <div class="clearfix wrapper dk nav-user hidden-xs">
                                        <div class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb avatar pull-left m-r"> <img src="images/a0.jpg"> <i class="on md b-black"></i> </span> <span class="hidden-nav-xs clear"> <span class="block m-t-xs"> <strong class="font-bold text-lt"><?php
                                                            $sql = "SELECT fname,country FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                                                            if ($result = mysqli_query($con, $sql)) {

                                                                /* fetch associative array */
                                                                while ($row = mysqli_fetch_row($result)) {
                                                                    print $row[0];
                                                                    $coun = $row[1];
                                                                }
                                                            }
                                                            ?></strong> <b class="caret"></b> </span> <span class="text-muted text-xs block">Administrador</span> </span> </a>
                                        </div>
                                    </div>
                                    <!-- nav -->
                                    <nav class="nav-primary hidden-xs">
                                        <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">MENU DE ADMINISTRAÇÃO</div>
                                        <?php
                                        include_once 'menu.php';
                                        ?>
                                        <div class="line dk hidden-nav-xs"></div>


                                    </nav>
                                    <!-- / nav -->
                                </div>
                            </section>
                            <footer class="footer hidden-xs no-padder text-center-nav-xs"> <a href="logout.php" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs"> <i class="i i-logout"></i> </a> <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs"> <i class="i i-circleleft text"></i> <i class="i i-circleright text-active"></i> </a> </footer>
                        </section>
                    </aside>
                    <!-- /.aside -->
                    <section id="content">
                        <div class="tab-content">
                            <div class="tab-pane active" id="home">


                                <div class="panel-body">


                                    <div class="table-responsive">
                                        <h1>Materiais</h1>
                                        <table class="table table-striped b-t b-light">
                                            <thead>
                                                <tr>

                                                    <th width="5%">ID</th>
                                                    <th width="15%">Nome</th>
                                                    <th width="30%">Descricao</th>
                                                    <th width="15%">Status</th>
                                                    <th width="15%">Tipo</th>
                                                    <th width="15%">Ação</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php

                                                function status($status) {

                                                    if ($status == 1) {
                                                        $status = 'Ativo';
                                                    } else {
                                                        $status = 'Inativo';
                                                    }
                                                    return $status;
                                                }

                                                function tipo($status) {

                                                    if ($status == 1) {
                                                        $status = 'Link';
                                                    }
                                                    if ($status == 2) {
                                                        $status = 'Video do youtube';
                                                    } if ($status == 3) {
                                                        $status = 'Download';
                                                    }
                                                    return $status;
                                                }

                                                $usrNm = $_SESSION['adminidusername'];
                                                $query = "SELECT * FROM  materiais  ORDER BY id DESC";


                                                $result = mysqli_query($con, $query);
                                                $i = 0;
                                                while ($row = mysqli_fetch_array($result)) {

                                                    extract($row);
                                                    if ($tipo == 1) {
                                                        $linkMaterial = "<a href='" . $link . "'>Visitar Link</a>";
                                                    } if ($tipo == 2) {
                                                        $linkMaterial = "<a class='verVideo' data-video='" . $link . "' data-titulo='" . $nome . "'>Ver Video</a>";
                                                    } if ($tipo == 3) {
                                                        $linkMaterial = "<a href='" . URL . '/downloads/' . $link . "'>Download</a>";
                                                    }
                                                    $status = status($status);
                                                    $tipo = tipo($tipo);
                                                    print "<tr>
				  
				  <td>
				  $id
				  </td>
				  <td>
				  $nome
				  </td>
				  <td>
				   $descricao 
				  </td>
                                  <td>
                                  $status
                                  </td>
                                   <td>
                                  $tipo
                                  </td>
                                    <td>
                                  $linkMaterial
                                      <a href='editarMaterial.php?id=$id'>Editar</a>
                                  </td>
				  
				  
				

				  
				  </tr>";
                                                }
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>




                                </div>

                            </div>



                        </div>
                        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section>
                </section>
            </section>
        </section>
        <div id="modalVideo" class="modal fade" role="dialog">

            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modalVideoTitulo"></h4>
                    </div>
                    <div class="modal-body" id="modalVideoLink">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>

                </div>

            </div>
        </div>

        <!-- Bootstrap -->
        <!-- App -->
        <script src="js/app.v1.js"></script>
        <script src="js/jquery.ui.touch-punch.min.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="js/app.plugin.js"></script>
        <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready(function () {
                $('table').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true,
                    "order": [[0, "desc"]],
                    "language": {
                        "sEmptyTable": "Nenhum registro encontrado",
                        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "_MENU_ resultados por página",
                        "sLoadingRecords": "Carregando...",
                        "sProcessing": "Processando...",
                        "sZeroRecords": "Nenhum registro encontrado",
                        "sSearch": "Pesquisar",
                        "oPaginate": {
                            "sNext": "Próximo",
                            "sPrevious": "Anterior",
                            "sFirst": "Primeiro",
                            "sLast": "Último"
                        },
                        "oAria": {
                            "sSortAscending": ": Ordenar colunas de forma ascendente",
                            "sSortDescending": ": Ordenar colunas de forma descendente"
                        }
                    }
                });
            });
            $(".verVideo").click(function () {
                video = $(this).attr('data-video');
                video = '<iframe width="560" height="315" src="' + video + '" frameborder="0" allowfullscreen></iframe>';
                titulo = $(this).attr('data-titulo');
                $("#modalVideoTitulo").html(titulo);
                $("#modalVideoLink").html(video);
                $("#modalVideo").modal();

            })
        </script>
    </body>
</html>