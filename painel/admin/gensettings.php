<?php
include_once ("z_db.php");
// Inialize session
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8" />
        <title><?= TITULO_DEF ?></title>
        <meta name="description" content="Sistema para Marketing Multinível" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
        <link rel="icon" href="images/favicon.png" type="favicon" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
    </head>
    <body class="">
        <section class="vbox">
            <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
                <div class="navbar-header aside-md dk"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="fa fa-bars"></i> </a> <a href="dashboard.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm"> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i> </a> </div>


                <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">

                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="images/a0.jpg"> </span> <?php
                            $sql = "SELECT fname FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                            if ($result = mysqli_query($con, $sql)) {

                                /* fetch associative array */
                                while ($row = mysqli_fetch_row($result)) {
                                    print $row[0];
                                }
                            }
                            ?><b class="caret"></b> </a>
                        <ul class="dropdown-menu animated fadeInRight">
                            <span class="arrow top"></span>
                            <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
                        </ul>
                    </li>
                </ul>
            </header>
            <section>
                <section class="hbox stretch">
                    <!-- .aside -->
                    <aside class="bg-light aside-md hidden-print" id="nav">
                        <section class="vbox">
                            <section class="w-f scrollable">
                                <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-color="#333333">
                                    <div class="clearfix wrapper dk nav-user hidden-xs">
                                        <div class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb avatar pull-left m-r"> <img src="images/a0.jpg"> <i class="on md b-black"></i> </span> <span class="hidden-nav-xs clear"> <span class="block m-t-xs"> <strong class="font-bold text-lt"><?php
                                                            $sql = "SELECT fname,country FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                                                            if ($result = mysqli_query($con, $sql)) {

                                                                /* fetch associative array */
                                                                while ($row = mysqli_fetch_row($result)) {
                                                                    print $row[0];
                                                                    $coun = $row[1];
                                                                }
                                                            }
                                                            ?></strong> <b class="caret"></b> </span> <span class="text-muted text-xs block">Administrador</span> </span> </a>
                                        </div>
                                    </div>
                                    <!-- nav -->
                                    <nav class="nav-primary hidden-xs">
                                        <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">MENU DE ADMINISTRAÇÃO</div>
                                        <ul class="nav nav-main" data-ride="collapse">
                                            <li class="active" > <a href="dashboard.php" class="auto"><i class="i i-statistics icon"></i> <span>Visão Geral</span> </a> </li>
                                            <li> <a href="gensettings.php" class="auto"> <i class="fa fa-cog" aria-hidden="true"></i> <span>Configurações Gerais</span> </a> </li>
                                            <li> <a href="emailsettings.php" class="auto"> <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Gerenciar e-mails</span> </a> </li>
                                            <li> <a href="pacsettings.php" class="auto"> <i class="fa fa-tasks" aria-hidden="true"></i> <span>Gerenciar Planos</span> </a> </li>
                                            <li> <a href="notifications.php" class="auto"><i class="fa fa-tags" aria-hidden="true"></i> <span>Notificações</span> </a> </li>
                                            <li> <a href="users.php" class="auto"><i class="fa fa-user"></i> <span>Usuários</span> </a> </li>
                                            <li> <a href="payments.php" class="auto"><i class="fa fa-money" aria-hidden="true"></i> <span>Pagamentos via Paypal</span> </a> </li>
                                            <li> <a href="paymentscod.php" class="auto"><i class="fa fa-exchange" aria-hidden="true"></i> <span>Pagamentos em dinheiro </span> </a> </li>
                                            <li> <a href="payrequest.php" class="auto"><i class="fa fa-refresh" aria-hidden="true"></i> <span>Requisições de Pagamento </span> </a> </li>
                                            <li> <a href="renewpaymentscod.php" class="auto"><i class="fa fa-plus" aria-hidden="true"></i> <span>Renovações </span> </a> </li>

                                        </ul>
                                        <div class="line dk hidden-nav-xs"></div>


                                    </nav>
                                    <!-- / nav -->
                                </div>
                            </section>
                            <footer class="footer hidden-xs no-padder text-center-nav-xs"> <a href="logout.php" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs"> <i class="i i-logout"></i> </a> <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs"> <i class="i i-circleleft text"></i> <i class="i i-circleright text-active"></i> </a> </footer>
                        </section>
                    </aside>
                    <!-- /.aside -->
                    <section id="content">
                        <section class="vbox">
                            <section class="scrollable wrapper">
                                <div class="row">

                                    <div class="col-sm-12 portlet">
                                        <section class="panel panel-success portlet-item">
                                            <header class="panel-heading"> Configurações Gerais</header>
                                            <section class="panel panel-default">
                                                <ul class="nav nav-tabs nav-justified">
                                                    <?php
                                                    $query = "SELECT * FROM  settings";


                                                    $result = mysqli_query($con, $query);
                                                    $i = 0;
                                                    while ($row = mysqli_fetch_array($result)) {

                                                        $email = "$row[email]";
                                                        $wlink = "$row[wlink]";
                                                        $ide = "$row[invoicedetails]";
                                                        $coname = "$row[coname]";
                                                        $fblink = "$row[fblink]";
                                                        $tlink = "$row[twitterlink]";
                                                        $pid = "$row[paypalid]";
                                                        $sno = "$row[sno]";
                                                        $ftrtext = "$row[footer]";
                                                        $hdrtext = "$row[header]";
                                                        $maintain = "$row[maintain]";
                                                        $payzaid = "$row[payzaid]";
                                                        $solidtrustid = "$row[solidtrustid]";
                                                        $solidbuttonid = "$row[solidbutton]";

                                                        $saque = "$row[saque]";
                                                        $valorCota = "$row[valorCota]";
                                                        $solidtrustid = "$row[solidtrustid]";

                                                        $iugu_status = "$row[iugu_status]";
                                                        $gnt_status = "$row[gnt_status]";

                                                        $iugu_token = "$row[iugu_token]";
                                                        $gnt_status = "$row[gnt_status]";
                                                        $gnt_client = "$row[gnt_client]";
                                                        $gnt_secret = "$row[gnt_secret]";
                                                        $tax_renovacao = "$row[tax_renovacao]";
                                                        $tax_saque = "$row[tax_saque]";
                                                        $dia_saque = "$row[dia_saque]";
                                                    }
                                                    ?>  
                                                </ul>
                                                </header>
                                                <div class="panel-body">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="home">


                                                            <div class="panel-body">
                                                                <form action="updategensettings.php" method="post">


                                                                    <div class="form-group">


                                                                        <label>Link do website</label>
                                                                        <input type="text" value="<?php print $wlink ?>" class="form-control" placeholder="Link do seu Backoffice" name="wlink" >
                                                                    </div>
                                                                    <input type="hidden" value="<?php print $sno ?>"  name="sno">
                                                                    <div class="form-group">
                                                                        <label>Nome da empresa</label>
                                                                        <input type="text" value="<?php print $coname ?>" class="form-control" placeholder="Nome da empresa" name="coname">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Endereço</label>
                                                                        <input type="textarea" value="<?php print $ide ?>" class="form-control" placeholder="Endereço" name="codetail">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>Email corporativo</label>
                                                                        <input type="email" value="<?php print $email ?>" class="form-control" placeholder="E-mail de contato" name="coemail" >
                                                                    </div>

                                                                    <br>

                                                                    <div class="form-group">
                                                                        <br>
                                                                        <?php
                                                                        $DB->where('id', 2);
                                                                        $alwdcash = $DB->getOne('paymentgateway')['status'];
                                                                        ?>

                                                                        <label>Ativar pagamento via depósito? </label>
                                                                        <select name="alwdcash" class="form-control" required>
                                                                            <?php
                                                                            if ($alwdcash == '1') {
                                                                                echo "<option value = '1' selected>Sim</option>";
                                                                            }
                                                                            if ($alwdcash == '0') {
                                                                                echo "<option value = '0' selected>Não</option>";
                                                                            }
                                                                            ?>                                                  
                                                                            <option value='1'>Sim</option>
                                                                            <option value='0'>Não</option>

                                                                        </select>   
                                                                    </div>			  

                                                                    <br>
                                                                    <div class="form-group">
                                                                        <label>Fanpage do Facebook</label>
                                                                        <input type="text" value="<?php print $fblink ?>" class="form-control" placeholder="Página do facebook" name="fblink" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Perfil do Twitter</label>
                                                                        <input type="text" value="<?php print $tlink ?>" class="form-control" placeholder="Página do Twitter" name="twitterlink" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Texto do Cabeçalho</label>
                                                                        <input type="text" value="<?php print $hdrtext ?>" class="form-control" placeholder="Texto do Cabeçalho" name="hdrtext" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Texto do Rodapé</label>
                                                                        <input type="text" value="<?php print $ftrtext ?>" class="form-control" placeholder="Texto do Roda-pé" name="ftrtext" >
                                                                    </div>
                                                                    <div class="form-group">


                                                                        <label>Status do Sistema</label>
                                                                        <input type="text" value="<?php
                                                                        if ($maintain == 0) {
                                                                            print "Online";
                                                                        } else if ($maintain == 1) {
                                                                            print "Offline";
                                                                        } else if ($maintain == 2) {
                                                                            print "Website Is Under Maintenance And Registrations Are Disabled For Members";
                                                                        } else if ($maintain == 3) {
                                                                            print "Website Is Under Maintenance, Both Registrations And Login Are Disabled For Members";
                                                                        }
                                                                        ?>" class="form-control" name="sddssdsddsdsdss" disabled >
                                                                    </div>

                                                                    <label>Alterar Status do Sistema</label> 
                                                                    <select name="maintain"class="form-control" >

                                                                        <option value='0'>Online</option> 
                                                                        <option value='1'>Desativar Login</option> 
                                                                        <option value='2'>Desativar Cadastro</option> 
                                                                        <option value='3'>Desativar Pagamento</option> 
                                                                    </select>
                                                                    <div class="form-group">
                                                                        <br>

                                                                        <label>Saque? </label>
                                                                        <select name="saque" class="form-control" required>
                                                                            <option value='<?= $saque ?>' selected=""><?= $saque ?></option>
                                                                            <option value='Ativo'>Ativo</option>
                                                                            <option value='Inativo'>Inativo</option>
                                                                        </select>   
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Taxa do saque</label>
                                                                        <input type="number" step="any" value="<?php print $tax_saque ?>" class="form-control" placeholder="Valor em porcentagem.Ex: 10" name="tax_saque" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Taxa de renovação com saldo</label>
                                                                        <input type="number" step="any" value="<?php print $tax_renovacao ?>" class="form-control" placeholder="Valor em porcentagem.Ex: 10" name="tax_renovacao" >
                                                                    </div>
                                                                    <?php

                                                                    function verificarDia($dia, $dia_saque) {
                                                                        foreach (json_decode($dia_saque) as $value) {
                                                                            if ($dia == $value) {
                                                                                return "checked=''";
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <div class="form-group">
                                                                        <br>

                                                                        <label>Dias de saque? </label>
                                                                        <labe>  <input name="diasSaque[]" value="0" type="checkbox" <?= verificarDia(0, $dia_saque) ?>>Domingo</labe>
                                                                        <labe>  <input name="diasSaque[]" value="1" type="checkbox" <?= verificarDia(1, $dia_saque) ?>>Segunda Feira</labe>
                                                                        <labe> <input name="diasSaque[]" value="2" type="checkbox" <?= verificarDia(2, $dia_saque) ?>>Terça Feira </labe>
                                                                        <labe>  <input name="diasSaque[]" value="3" type="checkbox" <?= verificarDia(3, $dia_saque) ?>>Quarta Feira</labe>
                                                                        <labe>  <input name="diasSaque[]" value="4" type="checkbox" <?= verificarDia(4, $dia_saque) ?>>Quinta Feira</labe>
                                                                        <labe>  <input name="diasSaque[]" value="5" type="checkbox" <?= verificarDia(5, $dia_saque) ?>>Sexta Feira</labe>
                                                                        <labe>  <input name="diasSaque[]" value="6" type="checkbox" <?= verificarDia(6, $dia_saque) ?>>Sábado</labe>

                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>Valor da cota</label>
                                                                        <input type="number" value="<?php print $valorCota ?>" class="form-control" placeholder="Ex: 10" name="valorCota" >
                                                                    </div>


                                                            </div>
                                                            &nbsp &nbsp <button type="submit" class="btn btn-sm btn-default" style="background-color:#ffd800; border-color:#ffd800;"> <font color="#ffffff"><b>Salvar Configurações</b> </font></button>
                                                            </form>
                                                        </div> 

                                                    </div>



                                                </div>
                                                </div>
                                            </section>
                                        </section>

                                    </div>
                                </div>
                            </section>
                        </section>
                        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section>
                </section>
            </section>
        </section>
        <!-- Bootstrap -->
        <!-- App -->
        <script src="js/app.v1.js"></script>
        <script src="js/jquery.ui.touch-punch.min.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="js/app.plugin.js"></script>
    </body>
</html>