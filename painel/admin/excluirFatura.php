<?php

include_once ("z_db.php");
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}
$DB->where('id', $DB->escape($_GET['id']));
$upda = $DB->update('pagamentos', array('status' => 55, 'user_operacao' => $_SESSION['adminidusername']));
if ($upda) {
    redirect('faturas.php?info=Operação realizada com sucesso!');
} else {
    redirect('faturas.php?info=Ocorreu um erro!');
}
?>