<?php

include_once ("z_db.php");
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}
$DB->where('id', $DB->escape($_GET['id']));
$DB->where('status', 1);
$pagamento = $DB->getOne('pagamentos');
if (isset($pagamento['id'])) {
    $operacao = explode('-', $pagamento['descricao']);
    if ($operacao[0] == 'Ativação') {
        $util->ativarUsr($pagamento['username']);
        $DB->where('id', $DB->escape($_GET['id']));
        $DB->update('pagamentos', ['status' => 3]);
        redirect('faturas.php?info=Operação realizada com sucesso!');
    } else if ($operacao[0] == 'Upgrade') {
        $util->ativarUpgrade($pagamento['username'],$pagamento['pacote']);
        $DB->where('id', $DB->escape($_GET['id']));
        $DB->update('pagamentos', ['status' => 3]);
        redirect('faturas.php?info=Operação realizada com sucesso!');
    } else if ($operacao[0] == 'Renovação') {
        $util->ativarRenovacao($pagamento['username']);
        $DB->where('id', $DB->escape($_GET['id']));
        $DB->update('pagamentos', ['status' => 3]);
        redirect('faturas.php?info=Operação realizada com sucesso!');
    } else {
        redirect('faturas.php?info=Ocorreu um erro!');
    }
} else {
    redirect('faturas.php?info=Ocorreu um erro!');
}
?>