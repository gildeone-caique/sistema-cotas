<?php
include_once ("z_db.php");
// Inialize session
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
          redirect('index.php');

}
$upid = mysqli_real_escape_string($con,$_POST['upackage']);
?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
<meta charset="utf-8" />
<title><?=TITULO_DEF?></title>
<meta name="description" content="Sistema para Marketing Multinível" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link rel="stylesheet" href="css/app.v1.css" type="text/css" />
<link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
<link rel="icon" href="images/favicon.png" type="favicon" />
<!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
</head>
<body class="">
<section class="vbox">
  <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
    <div class="navbar-header aside-md dk"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="fa fa-bars"></i> </a> <a href="dashboard.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm"> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i> </a> </div>
  
    
    <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">
      
      <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="images/a0.jpg"> </span> <?php
		  $sql="SELECT fname FROM  affiliateuser WHERE username='".$_SESSION['adminidusername']."'";
		  if ($result = mysqli_query($con, $sql)) {

    /* fetch associative array */
    while ($row = mysqli_fetch_row($result)) {
        print $row[0];
    }

}

   
	   
	   ?><b class="caret"></b> </a>
        <ul class="dropdown-menu animated fadeInRight">
          <span class="arrow top"></span>
          <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
        </ul>
      </li>
    </ul>
  </header>
  <section>
    <section class="hbox stretch">
      <!-- .aside -->
      <aside class="bg-light aside-md hidden-print" id="nav">
        <section class="vbox">
          <section class="w-f scrollable">
            <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-color="#333333">
              <div class="clearfix wrapper dk nav-user hidden-xs">
                <div class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb avatar pull-left m-r"> <img src="images/a0.jpg"> <i class="on md b-black"></i> </span> <span class="hidden-nav-xs clear"> <span class="block m-t-xs"> <strong class="font-bold text-lt"><?php
		  $sql="SELECT fname,country FROM  affiliateuser WHERE username='".$_SESSION['adminidusername']."'";
		  if ($result = mysqli_query($con, $sql)) {

    /* fetch associative array */
    while ($row = mysqli_fetch_row($result)) {
        print $row[0];
		$coun=$row[1];
    }

}

   
	   
	   ?></strong> <b class="caret"></b> </span> <span class="text-muted text-xs block">Administrador</span> </span> </a>
                </div>
              </div>
              <!-- nav -->
              <nav class="nav-primary hidden-xs">
                <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">MENU DE ADMINISTRAÇÃO</div>
                <ul class="nav nav-main" data-ride="collapse">
                      <li class="active" > <a href="dashboard.php" class="auto"><i class="i i-statistics icon"></i> <span>Visão Geral</span> </a> </li>
                      <li> <a href="gensettings.php" class="auto"> <i class="fa fa-cog" aria-hidden="true"></i> <span>Configurações Gerais</span> </a> </li>
					  <li> <a href="emailsettings.php" class="auto"> <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Gerenciar e-mails</span> </a> </li>
                      <li> <a href="pacsettings.php" class="auto"> <i class="fa fa-tasks" aria-hidden="true"></i> <span>Gerenciar Planos</span> </a> </li>
                      <li> <a href="notifications.php" class="auto"><i class="fa fa-tags" aria-hidden="true"></i> <span>Notificações</span> </a> </li>
                      <li> <a href="users.php" class="auto"><i class="fa fa-user"></i> <span>Usuários</span> </a> </li>
                      <li> <a href="payments.php" class="auto"><i class="fa fa-money" aria-hidden="true"></i> <span>Pagamentos via Paypal</span> </a> </li>
					  <li> <a href="paymentscod.php" class="auto"><i class="fa fa-exchange" aria-hidden="true"></i> <span>Pagamentos em dinheiro </span> </a> </li>
					  <li> <a href="payrequest.php" class="auto"><i class="fa fa-refresh" aria-hidden="true"></i> <span>Requisições de Pagamento </span> </a> </li>
					  <li> <a href="renewpaymentscod.php" class="auto"><i class="fa fa-plus" aria-hidden="true"></i> <span>Renovações </span> </a> </li>
				  
                </ul>
                <div class="line dk hidden-nav-xs"></div>
                
                
              </nav>
              <!-- / nav -->
            </div>
          </section>
          <footer class="footer hidden-xs no-padder text-center-nav-xs"> <a href="logout.php" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs"> <i class="i i-logout"></i> </a> <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs"> <i class="i i-circleleft text"></i> <i class="i i-circleright text-active"></i> </a> </footer>
        </section>
      </aside>
      <!-- /.aside -->
      <section id="content">
        <section class="vbox">
          <section class="scrollable wrapper">
            <div class="row">
              
              <div class="col-sm-12 portlet">
                <section class="panel panel-success portlet-item">
                  <header class="panel-heading"> Atualizar Planos </header>
                  <section class="panel panel-default">
                    <ul class="nav nav-tabs nav-justified">
                      <?php 
					  
					  $query="SELECT * FROM  packages where id=$upid"; 
 
 
 $result = mysqli_query($con,$query);

while($row = mysqli_fetch_array($result))
{
	
	$pname="$row[name]";
	$pdetail="$row[details]";
	$pprice="$row[price]";
	$pcurid="$row[currency]";
	$pckmpay="$row[mpay]";
	$pcktax="$row[tax]";
	$pcksbonus="$row[sbonus]";
	$pckactive="$row[active]";
	$p1="$row[level1]";
	$p2="$row[level2]";
	$p3="$row[level3]";
	$p4="$row[level4]";
	$p5="$row[level5]";
	$p6="$row[level6]";
	$p7="$row[level7]";
	$p8="$row[level8]";
	$p9="$row[level9]";
	$p10="$row[level10]";
	$p11="$row[level11]";
	$p12="$row[level12]";
	$p13="$row[level13]";
	$p14="$row[level14]";
	$p15="$row[level15]";
	$p16="$row[level16]";
	$p17="$row[level17]";
	$p18="$row[level18]";
	$p19="$row[level19]";
	$p20="$row[level20]";
	$validity="$row[validity]";
	}
					  
					  ?>  
                    </ul>
                  </header>
                  <div class="panel-body">
                    <div class="tab-content">
                      <div class="tab-pane active" id="home">
					  
					  
					  <div class="panel-body">
                    <form action="updatepcksettings.php" method="post">
					 <input type="hidden" value="<?php print $upid ?>" name="pckmainid">
					<div class="form-group">
                        <label>Status do Pacote - 1: Ativo | 2: Inativo</label>
                        <input type="text" value="<?php print $pckactive ?>" class="form-control" placeholder="Deseja tornar esse pacote Ativo ou Inativo?" name="pckact">
                      </div>
                      <div class="form-group">
                        <label>Nome do Pacote</label>
                        <input type="text" value="<?php print $pname ?>" class="form-control" placeholder="Nome do Pacote" name="pckname">
                      </div>
                      <div class="form-group">
                        <label>Detalhes</label>
                        <input type="textarea" value="<?php print $pdetail ?>" class="form-control" placeholder="Ex: Número de Cotas" name="pckdetail">
                      </div>
					  
					  <div class="form-group">
                        <label>Preço (Ex: 300)</label>
                        <input type="text" value="<?php print $pprice ?>" class="form-control" placeholder="Preço (Não coloque virgulas e pontos)" name="pckprice" >
                      </div>
					  
<div class="form-group">
                        <label>Taxa de cadastro (Ex: 20)</label>
                        <input type="text" value="<?php print $pcktax ?>" class="form-control" placeholder="Taxas (Não coloque virgulas e pontos)" name="pcktax" >
                      </div>

<div class="form-group">
                        <label>Valor mínimo para saque</label>
                        <input type="text" value="<?php print $pckmpay ?>" class="form-control" placeholder="Associados que aderirem a esse pacote só poderão realizar saque após completar o valor mínimo" name="pckmpay" >
                      </div>
					  
					   <div class="form-group">
                        <label>Bônus incentivo</label>
                        <input type="text" value="<?php print $pcksbonus ?>" class="form-control" placeholder="Ao se cadastrar, o usuário irá ganhar o valor do bônus incentivo" name="pcksbonus" >
                      </div>
					  
					  <div class="form-group">
					  <label>
            Moeda 
		  <select name="currency" class="form-control">
		  
		  <?php $query="SELECT id,name,code FROM  currency"; 
 
 
 $result = mysqli_query($con,$query);

while($row = mysqli_fetch_array($result))
{
	$id="$row[id]";
	$curname="$row[name]";
	$curcode="$row[code]";
	
  print "<option value='$curcode'>$curname - $curcode </option>";
  
  }
  ?>
 
</select>
</label> 

<div class="form-group">
                        <label>1° Nível</label>
                        <input type="text" value="<?php print $p1 ?>" class="form-control" placeholder="Apenas números" name="lev1">
                      </div>
					  <div class="form-group">
                        <label>2° Nível</label>
                        <input type="text" value="<?php print $p2 ?>" class="form-control" placeholder="Apenas números" name="lev2">
                      </div>
					  <div class="form-group">
                        <label>3° Nível</label>
                        <input type="text" value="<?php print $p3 ?>" class="form-control" placeholder="Apenas números" name="lev3">
                      </div>
					  <div class="form-group">
                        <label>4° Nível</label>
                        <input type="text" value="<?php print $p4 ?>" class="form-control" placeholder="Apenas números" name="lev4">
                      </div>
					  <div class="form-group">
                        <label>5° Nível</label>
                        <input type="text" value="<?php print $p5 ?>" class="form-control" placeholder="Apenas números" name="lev5">
                      </div>
					  <div class="form-group">
                        <label>6° Nível</label>
                        <input type="text" value="<?php print $p6 ?>" class="form-control" placeholder="Apenas números" name="lev6">
                      </div>
					  <div class="form-group">
                        <label>7° Nível</label>
                        <input type="text" value="<?php print $p7 ?>" class="form-control" placeholder="Apenas números" name="lev7">
                      </div>
					  <div class="form-group">
                       <label>8° Nível</label>
                        <input type="text" value="<?php print $p8 ?>" class="form-control" placeholder="Apenas números" name="lev8">
                      </div>
					  <div class="form-group">
                        <label>9° Nível</label>
                        <input type="text" value="<?php print $p9 ?>" class="form-control" placeholder="Apenas números" name="lev9">
                      </div>
					  <div class="form-group">
                        <label>10° Nível</label>
                        <input type="text" value="<?php print $p10 ?>" class="form-control" placeholder="Apenas números" name="lev10">
                      </div>
					  <div class="form-group">
                        <label>11° Nível</label>
                        <input type="text" value="<?php print $p11 ?>" class="form-control" placeholder="Apenas números" name="lev11">
                      </div>
					  <div class="form-group">
                        <label>12° Nível</label>
                        <input type="text" value="<?php print $p12 ?>" class="form-control" placeholder="Apenas números" name="lev12">
                      </div>
					  <div class="form-group">
                        <label>13° Nível</label>
                        <input type="text" value="<?php print $p13 ?>" class="form-control" placeholder="Apenas números" name="lev13">
                      </div>
					  <div class="form-group">
                       <label>14° Nível</label>
                        <input type="text" value="<?php print $p14 ?>" class="form-control" placeholder="Apenas números" name="lev14">
                      </div>
					  <div class="form-group">
                        <label>15° Nível</label>
                        <input type="text" value="<?php print $p15 ?>" class="form-control" placeholder="Apenas números" name="lev15">
                      </div>
					  <div class="form-group">
                       <label>16° Nível</label>
                        <input type="text" value="<?php print $p16 ?>" class="form-control" placeholder="Apenas números" name="lev16">
                      </div>
					  <div class="form-group">
                       <label>17° Nível</label>
                        <input type="text" value="<?php print $p17 ?>" class="form-control" placeholder="Apenas números" name="lev17">
                      </div>
					  <div class="form-group">
                        <label>18° Nível</label>
                        <input type="text" value="<?php print $p18 ?>" class="form-control" placeholder="Apenas números" name="lev18">
                      </div>
					  <div class="form-group">
                       <label>19° Nível</label>
                        <input type="text" value="<?php print $p19 ?>" class="form-control" placeholder="Apenas números" name="lev19">
                      </div>
					  
					  <div class="form-group">
                       <label>20° Nível</label>
                        <input type="text" value="<?php print $p20 ?>" class="form-control" placeholder="Apenas números" name="lev20">
                      </div>
					  <div class="form-group">
                       <label>Válidade do Pacote (em dias)</label>
                        <input type="text" value="<?php print $validity ?>" class="form-control" placeholder="Após quanto tempo o usuário terá que renovar o pagamento da sua adesão?" name="renewdays">
                      </div>
					  
					  
					  
					  
					  
</div>
                      
<button type="submit" class="btn btn-sm btn-default" style="background-color:#ffd800; border-color:#ffd800;"> <font color="#ffffff"><b>Atualizar Pacote</b> </font></button>
                    </form>
                  </div>
					  
					  </div>
                      
                      
                      
                    </div>
                  </div>
                </section>
                </section>
                
              </div>
            </div>
          </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section>
    </section>
  </section>
</section>
<!-- Bootstrap -->
<!-- App -->
<script src="js/app.v1.js"></script>
<script src="js/jquery.ui.touch-punch.min.js"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="js/app.plugin.js"></script>
</body>
</html>