<?php

header("refresh:1;url=gensettings.php");
include('z_db.php'); //connection details
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}
$status = "OK"; //initial status
$msg = "";
//fetching details through post method
$wlink = mysqli_real_escape_string($con, $_POST['wlink']);
$coname = mysqli_real_escape_string($con, $_POST['coname']);
$coemail = mysqli_real_escape_string($con, $_POST['coemail']);
$codetail = mysqli_real_escape_string($con, $_POST['codetail']);
$payemail = mysqli_real_escape_string($con, $_POST['payemail']);
$fb = mysqli_real_escape_string($con, $_POST['fblink']);
$tw = mysqli_real_escape_string($con, $_POST['twitterlink']);
$sno = mysqli_real_escape_string($con, $_POST['sno']);
$maintain = mysqli_real_escape_string($con, $_POST['maintain']);

$tax_saque = mysqli_real_escape_string($con, $_POST['tax_saque']);
$tax_renovacao = mysqli_real_escape_string($con, $_POST['tax_renovacao']);

$hdrtext = mysqli_real_escape_string($con, $_POST['hdrtext']);
$ftrtext = mysqli_real_escape_string($con, $_POST['ftrtext']);
$payzaid = mysqli_real_escape_string($con, $_POST['payzaid']);
$solidid = mysqli_real_escape_string($con, $_POST['solidid']);
$solidbutton = mysqli_real_escape_string($con, $_POST['solidbuttonid']);

$alwdpaypal = mysqli_real_escape_string($con, $_POST['alwdpaypal']);
$alwdpayment = mysqli_real_escape_string($con, $_POST['alwdpayment']);
$alwdpayza = mysqli_real_escape_string($con, $_POST['alwdpayza']);
$alwdsolid = mysqli_real_escape_string($con, $_POST['alwdsolid']);
$alwdcash = mysqli_real_escape_string($con, $_POST['alwdcash']);
$saque = mysqli_real_escape_string($con, $_POST['saque']);
$valorCota = mysqli_real_escape_string($con, $_POST['valorCota']);

$gnt_client = mysqli_real_escape_string($con, $_POST['gnt_client']);
$gnt_status = mysqli_real_escape_string($con, $_POST['gnt_status']);
$gnt_secret = mysqli_real_escape_string($con, $_POST['gnt_secret']);
$iugu_status = mysqli_real_escape_string($con, $_POST['ugu_status']);
$iugu_token = mysqli_real_escape_string($con, $_POST['iugu_token']);
$dia_saque = json_encode($_POST['diasSaque']);

if ($valorCota < 0) {
    $msg = $msg . "O valor da cota deve ser maior que 0.<BR>";
    $status = "NOTOK";
}
if (strlen($saque) < 4) {
    $msg = $msg . "Selecione um status válido.<BR>";
    $status = "NOTOK";
}


if (strlen($wlink) < 2) {
    $msg = $msg . "Textbox cannot be empty.<BR>";
    $status = "NOTOK";
}

if (strlen($coname) < 4) { //checking if body is greater then 4 or not
    $msg = $msg . "Textbox cannot be empty.<BR>";
    $status = "NOTOK";
}

if (strlen($coemail) < 4) { //checking if body is greater then 4 or not
    $msg = $msg . "Textbox cannot be empty.<BR>";
    $status = "NOTOK";
}
if (strlen($codetail) < 4) { //checking if body is greater then 4 or not
    $msg = $msg . "Textbox cannot be empty.<BR>";
    $status = "NOTOK";
}


if (strlen($ftrtext) < 1) { //checking if body is greater then 4 or not
    $msg = $msg . "Header Text Can Not Be Empty.<BR>";
    $status = "NOTOK";
}

if ($status == "OK") {
    $res1 = mysqli_query($con, "update settings set dia_saque='$dia_saque',tax_renovacao='$tax_renovacao',tax_saque='$tax_saque',iugu_status='$iugu_status',iugu_token='$iugu_token',gnt_status='$gnt_status',gnt_client='$gnt_client',gnt_secret='$gnt_secret',header='$hdrtext', footer='$ftrtext',saque='$saque',valorCota='$valorCota',wlink='$wlink',coname='$coname',invoicedetails='$codetail',email='$coemail',fblink='$fb',twitterlink='$tw',paypalid='$payemail', maintain='$maintain', alwdpayment='2', payzaid='$payzaid', solidtrustid='$solidid', solidbutton='$solidbutton' where sno='$sno'");
    $res1 = mysqli_query($con, "update paymentgateway set status='0' where id=1");
    $res1 = mysqli_query($con, "update paymentgateway set status='0' where id=3");
    $res1 = mysqli_query($con, "update paymentgateway set status='0' where id=4");
    $res1 = mysqli_query($con, "update paymentgateway set status='$alwdcash' where id=2");

    if ($res1) {
        print "Settings updated...!!! Redirecting...";
    } else {
        print "error!!!! try again later or ask for help from your administrator!!!! Redirecting...";
    }
} else {

    echo "<font face='Verdana' size='2' color=red>$msg</font><br><input type='button' value='Retry' onClick='history.go(-1)'>"; //printing errors
}
?>
