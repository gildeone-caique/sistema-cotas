<?php
include_once ("z_db.php");
// Inialize session
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8" />
        <title><?= TITULO_DEF ?></title>
        <meta name="description" content="Sistema para Marketing Multinível" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
        <link rel="icon" href="images/favicon.png" type="favicon" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
    </head>
    <body class="">
        <section class="vbox">
            <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
                <div class="navbar-header aside-md dk"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="fa fa-bars"></i> </a> <a href="dashboard.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm"> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i> </a> </div>


                <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">

                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="images/a0.jpg"> </span> <?php
                            $sql = "SELECT fname FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                            if ($result = mysqli_query($con, $sql)) {

                                /* fetch associative array */
                                while ($row = mysqli_fetch_row($result)) {
                                    print $row[0];
                                }
                            }
                            ?><b class="caret"></b> </a>
                        <ul class="dropdown-menu animated fadeInRight">
                            <span class="arrow top"></span>
                            <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
                        </ul>
                    </li>
                </ul>
            </header>
            <section>
                <section class="hbox stretch">
                    <!-- .aside -->
                    <aside class="bg-light aside-md hidden-print" id="nav">
                        <section class="vbox">
                            <section class="w-f scrollable">
                                <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-color="#333333">
                                    <div class="clearfix wrapper dk nav-user hidden-xs">
                                        <div class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb avatar pull-left m-r"> <img src="images/a0.jpg"> <i class="on md b-black"></i> </span> <span class="hidden-nav-xs clear"> <span class="block m-t-xs"> <strong class="font-bold text-lt"><?php
                                                            $sql = "SELECT fname,country FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                                                            if ($result = mysqli_query($con, $sql)) {

                                                                /* fetch associative array */
                                                                while ($row = mysqli_fetch_row($result)) {
                                                                    print $row[0];
                                                                    $coun = $row[1];
                                                                }
                                                            }
                                                            ?></strong> <b class="caret"></b> </span> <span class="text-muted text-xs block">Administrador</span> </span> </a>
                                        </div>
                                    </div>
                                    <!-- nav -->
                                    <nav class="nav-primary hidden-xs">
                                   <?php
                                        include('includes/menu.php');
                                        ?>
                                    </nav>
                                    <!-- / nav -->
                                </div>
                            </section>
                            <footer class="footer hidden-xs no-padder text-center-nav-xs"> <a href="logout.php" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs"> <i class="i i-logout"></i> </a> <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs"> <i class="i i-circleleft text"></i> <i class="i i-circleright text-active"></i> </a> </footer>
                        </section>
                    </aside>
                    <!-- /.aside -->
                    <section id="content">
                        <section class="vbox">
                            <section class="scrollable wrapper">
                                <div class="row">

                                    <div class="col-sm-12 portlet">
                                        <section class="panel panel-success portlet-item">
                                            <header class="panel-heading"> Adiministrar notificações </header>
                                            <section class="panel panel-default">
                                                <header class="panel-heading bg-light">
                                                    <ul class="nav nav-tabs nav-justified">
                                                        <li class="active"><a href="#home" data-toggle="tab">Adicionar notificação</a></li>
                                                        <li><a href="#profile" data-toggle="tab">Remover notificação</a></li>

                                                    </ul>
                                                </header>
                                                <div class="panel-body">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="home">


                                                            <div class="panel-body">
                                                                <form action="postnoti.php" method="post" onsubmit="return validarForm();">
                                                                    <div class="form-group">
                                                                        <label>Título</label>
                                                                        <input type="text" class="form-control" placeholder="Máximo de 20 palavras" name="notihead">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Corpo</label><br/>
                                                                        <textarea rows="8" cols="125" name="notibody" class="form-control"></textarea>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Username(deixe vazio para enviar para todos os usuários)</label>
                                                                        <input type="text" class="form-control"  name="username">
                                                                    </div>







                                                                    <button type="submit" class="btn btn-sm btn-default" style="background-color:#ffd800; border-color:#ffd800;"> <font color="#ffffff"><b>Adicionar notificação</b> </font></button>
                                                                </form>
                                                            </div>

                                                        </div>
                                                        <div class="tab-pane" id="profile"><form action="deletenoti.php" method="post" align="center">
                                                                <div class="form-group">
                                                                    <label>
                                                                        <select name="notisub" class="form-control">
                                                                            <?php
                                                                            $query = "SELECT id,subject,posteddate,user_id FROM  notifications where valid=1";


                                                                            $result = mysqli_query($con, $query);

                                                                            while ($row = mysqli_fetch_array($result)) {
                                                                                $id = "$row[id]";
                                                                                $notisubj = "$row[subject]";
                                                                                $notidate = "$row[posteddate]";
                                                                                $username = "$row[user_id]";


                                                                                print "<option value='$id'>$notisubj($username) | Dated - $notidate </option>";
                                                                            }
                                                                            ?>

                                                                        </select>
                                                                </div>



                                                                &nbsp &nbsp <button type="submit" class="btn btn-sm btn-default" style="background-color:#ffd800; border-color:#ffd800;"> <font color="#ffffff"><b>Remover notificação</b> </font></button>
                                                            </form></div>


                                                    </div>
                                                </div>
                                            </section>
                                        </section>

                                    </div>
                                </div>
                            </section>
                        </section>
                        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section>
                </section>
            </section>
        </section>
        <!-- Bootstrap -->
        <!-- App -->
        <script src="js/app.v1.js"></script>
        <script src="js/jquery.ui.touch-punch.min.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="js/app.plugin.js"></script>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>
                                                                    tinymce.init({selector: 'textarea', plugins: [
                                                                            "advlist autolink lists link image charmap print preview anchor",
                                                                            "searchreplace visualblocks code fullscreen",
                                                                            "insertdatetime media table contextmenu paste"
                                                                        ], setup: function (editor) {
                                                                            editor.on('change', function () {
                                                                                editor.save();
                                                                            });
                                                                        }
                                                                    });
                                                                    function validarForm() {
                                                                        conf = confirm("Você tem certeza?");
                                                                        if (conf) {
                                                                            return true;
                                                                        } else {
                                                                            return false;
                                                                        }
                                                                    }
        </script>
    </body>
</html>