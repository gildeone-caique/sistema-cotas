<?php
include_once ("z_db.php");
// Inialize session
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8" />
        <title><?= TITULO_DEF ?></title>
        <meta name="description" content="Sistema para Marketing Multinível" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
        <link rel="icon" href="images/favicon.png" type="favicon" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
    </head>
    <body class="">
        <section class="vbox">
            <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
                <div class="navbar-header aside-md dk"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="fa fa-bars"></i> </a> <a href="dashboard.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm"> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i> </a> </div>


                <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">

                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="images/a0.jpg"> </span> <?php
$sql = "SELECT fname FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
if ($result = mysqli_query($con, $sql)) {

    /* fetch associative array */
    while ($row = mysqli_fetch_row($result)) {
        print $row[0];
    }
}
?><b class="caret"></b> </a>
                        <ul class="dropdown-menu animated fadeInRight">
                            <span class="arrow top"></span>
                            <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
                        </ul>
                    </li>
                </ul>
            </header>
            <section>
                <section class="hbox stretch">
                    <!-- .aside -->
                    <aside class="bg-light aside-md hidden-print" id="nav">
                        <section class="vbox">
                            <section class="w-f scrollable">
                                <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-color="#333333">
                                    <div class="clearfix wrapper dk nav-user hidden-xs">
                                        <div class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb avatar pull-left m-r"> <img src="images/a0.jpg"> <i class="on md b-black"></i> </span> <span class="hidden-nav-xs clear"> <span class="block m-t-xs"> <strong class="font-bold text-lt"><?php
                            $sql = "SELECT fname,country FROM  affiliateuser WHERE username='" . $_SESSION['adminidusername'] . "'";
                            if ($result = mysqli_query($con, $sql)) {

                                /* fetch associative array */
                                while ($row = mysqli_fetch_row($result)) {
                                    print $row[0];
                                    $coun = $row[1];
                                }
                            }
?></strong> <b class="caret"></b> </span> <span class="text-muted text-xs block">Administrador</span> </span> </a>
                                        </div>
                                    </div>
                                    <!-- nav -->
                                    <nav class="nav-primary hidden-xs">
                                                            <?php
                                                            include('menu.php');
                                                            ?>
                                    </nav>
                                    <!-- / nav -->
                                </div>
                            </section>
                            <footer class="footer hidden-xs no-padder text-center-nav-xs"> <a href="logout.php" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs"> <i class="i i-logout"></i> </a> <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs"> <i class="i i-circleleft text"></i> <i class="i i-circleright text-active"></i> </a> </footer>
                        </section>
                    </aside>
                    <!-- /.aside -->
                    <section id="content">
                        <section class="vbox">
                            <section class="scrollable wrapper">
                                <div class="row">

                                    <div class="col-sm-12 portlet">
                                        <section class="panel panel-success portlet-item">
                                            <header class="panel-heading"> Gerenciar Planos </header>
                                            <section class="panel panel-default">
                                                <header class="panel-heading bg-light">
                                                    <ul class="nav nav-tabs nav-justified">
                                                        <li class="active"><a href="#home" data-toggle="tab">Criar Pacotes</a></li>
                                                        <li><a href="#profile" data-toggle="tab">Atualizar Pacotes</a></li>
                                                        <li><a href="#messages" data-toggle="tab">Desativar Pacotes</a></li>

                                                    </ul>
                                                </header>
                                                <div class="panel-body">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="home">


                                                            <div class="panel-body">
                                                                <form action="createpac.php" method="post">
                                                                    <div class="form-group">
                                                                        <label>Nome do pacote</label>
                                                                        <input type="text" class="form-control" placeholder="Nome do pacote" name="pckname">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Detalhes</label>
                                                                        <input type="textarea" class="form-control" placeholder="Ex: Número de Cotas" name="pckdetail">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>Preço (Ex: 300)</label>
                                                                        <input type="text" class="form-control" placeholder="Preço (Não coloque virgulas e pontos)" name="pckprice" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Taxa de cadastro (Ex: 20)</label>
                                                                        <input type="text" class="form-control" placeholder="Taxas (Não coloque virgulas e pontos)" name="pcktax" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>
                                                                            Moeda <br/>
                                                                            <select name="currency" class="form-control">
<?php
$query = "SELECT id,name,code FROM  currency";


$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result)) {
    $id = "$row[id]";
    $curname = "$row[name]";
    $curcode = "$row[code]";

    print "<option value='$curcode'>$curname - $curcode </option>";
}
?>

                                                                            </select>
                                                                        </label> 
                                                                        <br/>
                                                                        <div class="form-group">
                                                                            <br>

                                                                            <label>Valor mínimo para saque</label>
                                                                            <input type="text" class="form-control" placeholder="Associados que aderirem a esse pacote só poderão realizar saque após completar o valor mínimo" name="pckmpay" >
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label>Bônus incentivo</label>
                                                                            <input type="text" class="form-control" placeholder="Ao se cadastrar, o usuário irá ganhar o valor do bônus incentivo" name="pcksbonus" >
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label>1° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev1">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>2° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev2">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>3° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev3">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>4° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev4">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>5° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev5">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>6° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev6">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>7° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev7">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>8° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev8">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>9° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev9">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>10° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev10">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>11° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev11">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>12° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev12">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>13° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev13">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>14° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev14">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>15° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev15">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>16° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev16">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>17° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev17">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>18° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev18">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>19° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev19">
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label>20° Nível</label>
                                                                            <input type="text" class="form-control" placeholder="Apenas números" name="lev20">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Válidade do Pacote (em dias)</label>
                                                                            <input type="text" class="form-control" placeholder="Após quanto tempo o usuário terá que renovar o pagamento da sua adesão?" name="renewdays">
                                                                        </div>



                                                                    </div>
                                                                    <button type="submit" class="btn btn-sm btn-default" style="background-color:#ffd800; border-color:#ffd800;"> <font color="#ffffff"><b>Adicionar Pacote</b> </font></button>
                                                                </form>
                                                            </div>

                                                        </div>
                                                        <div class="tab-pane" id="profile"><form action="updatepck.php" method="post">
                                                                <div class="form-group" align="center">
                                                                    <label>
                                                                        <br>
                                                                        <br>
                                                                        <select name="upackage" class="form-control">
<?php
$query = "SELECT id,name,price,currency,tax FROM  packages";


$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result)) {
    $id = "$row[id]";
    $pname = "$row[name]";
    $pprice = "$row[price]";
    $pcur = "$row[currency]";
    $ptax = "$row[tax]";
    $total = $pprice + $ptax;
    print "<option value='$id'>$pname | Price - $pcur $total </option>";
}
?>

                                                                        </select>

                                                                        <br>

                                                                        <button type="submit" class="btn btn-sm btn-default" style="background-color:#ffd800; border-color:#ffd800;"> <font color="#ffffff"><b>Editar Pacote</b> </font></button>				  
                                                                </div>

                                                            </form></div>
                                                        <div class="tab-pane" id="messages" align="center">
                                                            <form action="deletepackage.php" method="post">
                                                                <label>
                                                                    <br>
                                                                    <br>
                                                                    <select name="packagedelid" class="form-control">
<?php
$query = "SELECT id,name,price,currency,tax FROM  packages where active=1";


$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result)) {
    $id = "$row[id]";
    $pname = "$row[name]";
    $pprice = "$row[price]";
    $pcur = "$row[currency]";
    $ptax = "$row[tax]";
    $total = $pprice + $ptax;
    print "<option value='$id'>$pname | Price - $pcur $total </option>";
}
?>

                                                                    </select>
                                                                </label> 
                                                                <br>
                                                                <br>
                                                                <button type="submit" class="btn btn-sm btn-default" style="background-color:#ffd800; border-color:#ffd800;"> <font color="#ffffff"><b>Excluir Pacote</b> </font></button>

                                                        </div>
                                                        </form>
                                                    </div>

                                                </div>
                                                </div>
                                            </section>
                                        </section>

                                    </div>
                                </div>
                            </section>
                        </section>
                        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section>
                </section>
            </section>
        </section>
        <!-- Bootstrap -->
        <!-- App -->
        <script src="js/app.v1.js"></script>
        <script src="js/jquery.ui.touch-punch.min.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="js/app.plugin.js"></script>
    </body>
</html>