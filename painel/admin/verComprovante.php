<?php

include_once ("z_db.php");
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}
$DB->where('id', $DB->escape($_GET['id']));
$upda = $DB->update('pagamentos', array('status_comprovante' => 1));
$DB->where('id', $DB->escape($_GET['id']));
$comprovante = $DB->getOne('pagamentos')['comprovante'];
if ($upda) {
    $compro = URL . '/comprovantes/' . $comprovante;
    redirect($compro);
} else {
    redirect('faturas.php?info=Ocorreu um erro!');
}
?>