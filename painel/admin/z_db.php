<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
include '../includes/util.php';

$util = new util();
$DB = $util->conn;
$con = $util->con1;

$util->exibirErros();

function redirect($url = 'index.php') {
    header("Location: $url");
    exit;
}

function addSaldo($DB, $username, $valor, $desc = 'Div. Lucro') {
    $today = date('y-m-d');

    $DB->where("username", $username);
    $userData = $DB->getOne('affiliateuser');
    $novoSaldo = $userData['tamount'] + $valor;
    $DB->where('Id', $userData['Id']);
    $DB->update('affiliateuser', array('tamount' => $novoSaldo));
    //insere extrato
    $data['valor'] = $valor;
    $data['beneficiado'] = $username;
    $data['descricao'] = $desc;
    $data['usuario'] = $_SESSION['adminidusername'];
    $data['data'] = $today;
    $DB->insert('extratos', $data);
}

function removeSaldo($DB, $username, $valor, $desc = 'Div. Lucro') {
    $today = date('y-m-d');

    $DB->where("username", $username);
    $userData = $DB->getOne('affiliateuser');
    $novoSaldo = $userData['tamount'] - $valor;
    $DB->where('Id', $userData['Id']);
    $DB->update('affiliateuser', array('tamount' => $novoSaldo));
    //insere extrato
    $valor = $valor * (-1);
    $data['valor'] = $valor;
    $data['beneficiado'] = $username;
    $data['descricao'] = $desc;
    $data['usuario'] = $_SESSION['adminidusername'];
    $data['data'] = $today;
    $DB->insert('extratos', $data);
}

function removeSaldo2($DB, $username, $valor, $desc = 'Div. Lucro') {
    $today = date('y-m-d');
    $DB->where("username", $username);
    $userData = $DB->getOne('affiliateuser');
    $novoSaldo = $userData['tamount'] - $valor;
    $DB->where('Id', $userData['Id']);
    $DB->update('affiliateuser', array('tamount' => $novoSaldo));
    //insere extrato
    $valor = $valor * (-1);
    $data['valor'] = $valor;
    $data['beneficiado'] = $username;
    $data['descricao'] = $desc;
    $data['usuario'] = 'administrador';
    $data['data'] = $today;
    $DB->insert('extratos', $data);
}

function ciclar($DB, $usr = '') {
    $today = date('y-m-d');

    if ($usr == '') {
        $usr = $_SESSION['username'];
    }
    $username = $DB->escape($usr);
    if (isset($username)) {
        $query = "SELECT SUM(`valor`) 
FROM  `extratos` 
WHERE STATUS =0
AND  `beneficiado` =  '{$username}'
AND (
 `descricao` =  'Div. Lucro'
OR  `descricao` =  'Indic. direita'
OR  `descricao` =  'Indic. Indireta'
) and valor>0";
        $res = $DB->query($query);
        $ganhos = $res[0]['SUM(`valor`)'];
//informações do usuário
        $DB->where('username', $username);
        $usrINfo = $DB->getOne('affiliateuser');
        $DB->where('id', $usrINfo['pcktaken']);
        $pacoteInfo = $DB->getOne('packages');
        $ganhoMaximo = $pacoteInfo['price'] * 3;

        if ($ganhos >= $ganhoMaximo) {
            if ($usrINfo['tamount'] >= $pacoteInfo['price']) {
                $DB->where('username', $username);
                $novoSaldo = $usrINfo['tamount'] - $pacoteInfo['price'];
                $DB->update('affiliateuser', array('tamount' => $novoSaldo));
                removeSaldo2($DB, $username, $pacoteInfo['price'], 'Automatic renovation');
                $query = "UPDATE extratos SET `status`=1
WHERE STATUS =0
AND  `beneficiado` =  '{$username}'
AND (
 `descricao` =  'Div. Lucro'
OR  `descricao` =  'Indic. direita'
OR  `descricao` =  'Indic. Indireta'
) and valor>0";
                $data['valor'] = $pacoteInfo['price'];
                $data['beneficiado'] = 'administrador';
                $data['descricao'] = 'Automatic renovation';
                $data['usuario'] = 'administrador';
                $data['data'] = $today;
                $DB->insert('extratos', $data);
                $DB->query($query);
            } else {
                $DB->where('username', $username);
                $DB->update('affiliateuser', array('active' => 0));
            }
        }
    }
}

header('Content-type: text/html; charset=UTF-8');
error_reporting(E_ALL);
ini_set("display_errors", 1);
define('SISTEMA_NOME', 'The Milionaire - Plataforma Virtual');
define('TITULO_DEF', 'The Milionaire - Plataforma Virtual');
define('DESC_DEF', 'The Milionaire  - Plataforma Virtual');
define('URL', 'http://localhost/ennixbrasil/painel');
?>