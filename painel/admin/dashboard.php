<?php

include_once ("z_db.php");
$configData = $DB->getOne('settings');
$today = date('y-m-d');


// Inialize session
set_time_limit(180);
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['adminidusername'])) {
    redirect('index.php');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['todo']) && (($_POST['todo']) == "divisao")) {
    if (1 == 1) {
        $valorDivisa = $_POST['valorDist'];
        if ($valorDivisa < 0 or $valorDivisa == 0 or $valorDivisa == '' or ! is_numeric($valorDivisa)) {
            echo 'Insira um número maior que 0';
            exit();
        }
        $DB->where('level', 2);
        $DB->where('active', 1);
        $DB->orderBy("Id", "asc");
        $users = $DB->get('affiliateuser');
        foreach ($users as $key => $value) {

            ciclar($DB, $value['username']);
        }
        $DB->where('level', 2);
        $DB->where('active', 1);
        $DB->orderBy("Id", "asc");
        $users = $DB->get('affiliateuser');
        foreach ($users as $key => $value) {
            $pacoteId = $value['pcktaken'];
            $DB->where('id', $pacoteId);
            $pacoteData = $DB->getOne('packages');
            $cota = $pacoteData['price'] / $configData['valorCota'];
            $valorDiv = $valorDivisa * $cota;
            addSaldo($DB, $value['username'], $valorDiv);
//procura usuarios que precisam renovar a conta
        }
        $DB->where('level', 2);
        $DB->where('active', 1);
        $DB->orderBy("Id", "asc");
        $users = $DB->get('affiliateuser');
        foreach ($users as $key => $value) {
            ciclar($DB, $value['username']);
        }
        $_SESSION['divisaoLucro'] = 1;
        redirect('dashboard.php?msg=Operação realizada com sucesso!');
        exit();
    } else {
        redirect('dashboard.php?msg=Operação não realizada!');
        exit();
    }
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8" />
        <title><?=SISTEMA_NOME?></title>
        <meta name="description" content="Sistema para Marketing Multinível" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
        <link rel="icon" href="images/favicon.png" type="favicon" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
    </head>
    <body class="">
        <section class="vbox">
            <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
                <div class="navbar-header aside-md dk"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="fa fa-bars"></i> </a> <a href="dashboard.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm"></a> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="fa fa-cog"></i> </a> </div>

                <?php
                if (isset($_GET['msg'])) {
                    $msg = $_GET['msg'];
                    echo "<script>alert('$msg'); </script> ";
                }
                ?>
                <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">

                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="images/a0.jpg"> </span> <?php
                            $sql = "SELECT fname FROM  affiliateuser WHERE username='" . $_SESSION ['adminidusername'] . "'";
                            if ($result = mysqli_query($con, $sql)) {

                                /* fetch associative array */
                                while ($row = mysqli_fetch_row($result)) {
                                    print utf8_encode($row[0]);
                                }
                            }
                            ?><b class="caret"></b> </a>
                        <ul class="dropdown-menu animated fadeInRight">
                            <span class="arrow top"></span>
                            <li> <a href="logout.php" data-toggle="ajaxModal" >Sair</a> </li>
                        </ul>
                    </li>
                </ul>
            </header>
            <section>
                <section class="hbox stretch">
                    <!-- .aside -->
                    <aside class="bg-light aside-md hidden-print" id="nav">
                        <section class="vbox">
                            <section class="w-f scrollable">
                                <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-color="#333333">
                                    <div class="clearfix wrapper dk nav-user hidden-xs">
                                        <div class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                                                <span class="thumb avatar pull-left m-r"> <img src="images/a0.jpg"> <i class="on md b-black"></i>
                                                </span> <span class="hidden-nav-xs clear"> 
                                                    <span class="block m-t-xs"> <strong class="font-bold text-lt"> <?php
                                                            $sql = "SELECT fname,country FROM  affiliateuser WHERE username='" . $_SESSION ['adminidusername'] . "'";
                                                            if ($result = mysqli_query($con, $sql)) {

                                                                /* fetch associative array */
                                                                while ($row = mysqli_fetch_row($result)) {
                                                                    print utf8_encode($row[0]);
                                                                    $coun = $row[1];
                                                                }
                                                            }
                                                            ?>
                                                        </strong> <b class="caret"></b> </span> <span class="text-muted text-xs block">Administrador</span> </span> </a>
                                        </div>
                                    </div>
                                    <!-- nav -->
                                    <nav class="nav-primary hidden-xs">
                                        <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">MENU DE ADMINISTRAÇÃO</div>
                                        <?php
                                        include_once 'menu.php';
                                        ?>
                                        <div class="line dk hidden-nav-xs"></div>


                                    </nav>
                                    <!-- / nav -->
                                </div>
                            </section>
                            <footer class="footer hidden-xs no-padder text-center-nav-xs"> <a href="logout.php" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs"> <i class="i i-logout"></i> </a> <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs"> <i class="i i-circleleft text"></i> <i class="i i-circleright text-active"></i> </a> </footer>
                        </section>
                    </aside>
                    <!-- /.aside -->
                    <section id="content">
                        <section class="hbox stretch">
                            <section>
                                <section class="vbox">
                                    <section class="scrollable padder">
                                        <section class="row m-b-md">
                                            <div class="col-sm-6">
                                                <h3 class="m-b-xs text-black">Painel Administrativo</h3>
                                                <small>Seja bem vindo, <?php
                                                    $sql = "SELECT fname FROM  affiliateuser WHERE username='" . $_SESSION ['adminidusername'] . "'";
                                                    if ($result = mysqli_query($con, $sql)) {

                                                        /* fetch associative array */
                                                        while ($row = mysqli_fetch_row($result)) {
                                                            print utf8_encode($row[0]);
                                                        }
                                                    }
                                                    ?>, <i class="fa fa-map-marker fa-lg text-primary"></i> <?php print $coun ?></small> </div>
                                            <div class="col-sm-6 text-right text-left-xs m-t-md">

                                                <a href="#" class="btn btn-icon b-2x btn-default btn-rounded hover"><i class="i i-bars3 hover-rotate"></i></a> <a href="#nav, #sidebar" class="btn btn-icon b-2x btn-info btn-rounded" data-toggle="class:nav-xs, show"><i class="fa fa-bars"></i></a> </div>
                                        </section>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel panel-default">
                                                    <div class="panel-body">
                                                        <?php
                                                        $query = "SELECT id,fname,email,doj,active,username,address,pcktaken,tamount FROM  affiliateuser where username = '" . $_SESSION ['adminidusername'] . "'";


                                                        $result = mysqli_query($con, $query);

                                                        while ($row = mysqli_fetch_array($result)) {
                                                            $aid = "$row[id]";
                                                            $regdate = "$row[doj]";
                                                            $name = "$row[fname]";
                                                            $address = "$row[address]";
                                                            $acti = "$row[active]";
                                                            $pck = "$row[pcktaken]";
                                                            $ear = "$row[tamount]";
                                                        }
                                                        ?> 
                                                        <?php
                                                        $result = mysqli_query($con, "SELECT count(*) FROM  affiliateuser where level = 2 and active = 1");
                                                        $row = mysqli_fetch_row($result);
                                                        $totalusers = $row[0];
                                                        ?> 
                                                        <footer class="panel-footer bg-info text-center">
                                                            <div class="row pull-out">
                                                                <div class="col-xs-6">
                                                                    <div class="padder-v"> <span class="m-b-xs h3 block text-white"><?php print $totalusers ?></span> <small class="text-muted">Total de usuários</small> </div>
                                                                </div>
                                                                <div class="col-xs-6 dk">
                                                                    <?php
                                                                    $query = "SELECT orderid,price FROM  paypalpayments";


                                                                    $result = mysqli_query($con, $query);
                                                                    $totalpayments = 0;
                                                                    while ($row = mysqli_fetch_array($result)) {
                                                                        $oid = "$row[orderid]";
                                                                        $result2 = mysqli_query($con, " SELECT count(*) FROM  affiliateuser where Id='$oid' and level = 2 and active = 1");
                                                                        $row22 = mysqli_fetch_row($result2);
                                                                        $toid = $row22[0];
                                                                        if ($toid == 1) {
                                                                            $totalpayments = $totalpayments + $ppayment = "$row[price]";
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <div class="padder-v"> <span class="m-b-xs h3 block text-white"><?php print $ear ?></span> <small class="text-muted">Faturamento total</small> </div>
                                                                </div>

                                                            </div>
                                                        </footer>
                                                </section>
                                            </div>
                                    </section>
                                </section>

                                <!-- / side content -->
                            </section>
                            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section>
                    </section>
                </section>
            </section>
            <!-- Bootstrap -->
            <!-- App -->
            <script src="js/app.v1.js"></script>
            <script src="js/charts/easypiechart/jquery.easy-pie-chart.js"></script>
            <script src="js/charts/sparkline/jquery.sparkline.min.js"></script>
            <script src="js/charts/flot/jquery.flot.min.js"></script>
            <script src="js/charts/flot/jquery.flot.tooltip.min.js"></script>
            <script src="js/charts/flot/jquery.flot.spline.js"></script>
            <script src="js/charts/flot/jquery.flot.pie.min.js"></script>
            <script src="js/charts/flot/jquery.flot.resize.js"></script>
            <script src="js/charts/flot/jquery.flot.grow.js"></script>
            <script src="js/charts/flot/demo.js"></script>
            <script src="js/calendar/bootstrap_calendar.js"></script>
            <script src="js/calendar/demo.js"></script>
            <script src="js/sortable/jquery.sortable.js"></script>
            <script src="js/app.plugin.js"></script>
    </body>
</html>