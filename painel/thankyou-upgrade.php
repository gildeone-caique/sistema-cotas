<?php
include_once ("z_db.php");
include 'includes/mercadopago.php';

error_reporting(E_ALL);
        ini_set("display_errors", 1);
// Inialize session
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['username'])) {
    redirect('index.php');
}

$pacote = $_GET['pacote'];




$sqlquerySet = "SELECT * FROM settings where sno=0"; //fetching website from databse
$recSet = mysqli_query($con, $sqlquerySet);
$rowSet = mysqli_fetch_array($recSet);
$settings = $rowSet; //assigning website address
if (!is_numeric($pacote)) {
    echo 'Ocorreu um erro';
    exit();
}
$DB->where('id', $pacote);
$pacoteData = $DB->getOne('packages');
if (!isset($pacoteData['id'])) {
    echo 'Ocorreu um erro';
    exit();
}

$userid = $_SESSION['username'];
$usrNm = mysqli_real_escape_string($con, $_SESSION['username']);
$DB->where('username', $usrNm);
$data = $DB->getOne('affiliateuser');


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $vencimento = date('Y-m-d', strtotime("+4 days"));
    $arr = ['pacote' => $pacoteData['id'], 'vencimento' => $vencimento, 'valor' => $pacoteData['price'], 'url' => '', 'status' => 1, 'ref' => 'Déposito/Transferência', 'username' => $userid, 'data_criacao' => date('Y-m-d'), 'descricao' => 'Upgrade - ' . $pacoteData['name']];
    $util->insertPagamento($arr);
    redirect('finalthankyoufree.php?username=' . $userid);
}
$userid = mysqli_real_escape_string($con, $_SESSION['username']);
if ($userid == "") {
    redirect('index.php');
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <style>html {
                overflow-y: scroll; 
            }</style>
        <meta charset="utf-8" />
        <title>Upgrade</title>
        <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v1.css" type="text/css" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->

    </head>
    <body style="overflow: scroll; background: url(images/background.png); max-width:100%" class="">
        <section id="content" class="m-t-lg wrapper-md animated fadeInDown">

            <div align=center> <a href="index.php"><img src="images/login.png" align="center" style="max-width:100%; padding-bottom:60px"></a>
                <section class="m-b-lg">
                    </br>
                    </br>
                    <?php
                    $mp = new mercadopago();
                    $urlMercado = $mp->pagamentoUrl($data, $pacoteData, 1);
                    ?>
                    <br>
                    <a  href='<?= $urlMercado ?>' class="btn btn-lg btn-primary btn-block"  >Pagar através do MercadoPago</a>
                    <br>
                    <?php
                    $query = "SELECT count(*) FROM  paymentgateway where name='Cash On Delivery' and status=1";

                    $result = mysqli_query($con, $query);
                    $row = mysqli_fetch_row($result);
                    $numrows = $row[0];

                    if ($numrows == 1) {
                        ?>
                        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>?pacote=<?= $_GET['pacote'] ?>" method="post">

                            <input type="submit" class="btn btn-lg btn-primary btn-block" value="Depósito/Transferência Bancária" >
                        </form>
                        <?php
                    }
                    ?>
            </div>
        </div>
    </section>
</div>
</section>
<!-- footer -->
<footer id="footer">
    <div class="text-center padder clearfix">
        <p> <small><?php
                $query = "SELECT footer from settings where sno=0";


                $result = mysqli_query($con, $query);

                while ($row = mysqli_fetch_array($result)) {
                    $footer = "$row[footer]";
                    print $footer;
                }
                ?></small> </p>
    </div>
</footer>
<!-- / footer -->
<!-- Bootstrap -->
<!-- App -->
<script src="js/app.v1.js"></script>
<script src="js/app.plugin.js"></script>
</body>
</html>