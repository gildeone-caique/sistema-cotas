-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 28/04/2016 às 23:48
-- Versão do servidor: 5.6.28-0ubuntu0.14.04.1
-- Versão do PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `linear`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `affiliateuser`
--

CREATE TABLE IF NOT EXISTS `affiliateuser` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` text NOT NULL,
  `fname` text NOT NULL,
  `address` text NOT NULL,
  `email` text NOT NULL,
  `referedby` varchar(15) NOT NULL DEFAULT 'none',
  `ipaddress` int(10) unsigned NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `active` int(11) NOT NULL,
  `doj` date NOT NULL,
  `country` text NOT NULL,
  `tamount` double NOT NULL DEFAULT '0',
  `payment` varchar(10) NOT NULL,
  `signupcode` text NOT NULL,
  `level` int(1) NOT NULL DEFAULT '2',
  `pcktaken` int(10) NOT NULL DEFAULT '0',
  `launch` int(1) NOT NULL DEFAULT '0',
  `expiry` date NOT NULL DEFAULT '2199-12-31',
  `bankname` varchar(250) NOT NULL DEFAULT 'Not Available',
  `accountname` varchar(250) NOT NULL DEFAULT 'Not Available',
  `accountno` double NOT NULL DEFAULT '0',
  `accounttype` int(11) NOT NULL DEFAULT '0',
  `ifsccode` varchar(100) NOT NULL DEFAULT 'Not Available',
  `getpayment` int(11) NOT NULL DEFAULT '1',
  `renew` int(1) NOT NULL DEFAULT '0',
  `qntdDivi` int(11) NOT NULL,
  `cpf` varchar(200) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `bannerid` double NOT NULL AUTO_INCREMENT,
  `bannerdesc` varchar(100) NOT NULL,
  `bannerhtml` text NOT NULL,
  `banneractive` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bannerid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `boletos`
--

CREATE TABLE IF NOT EXISTS `boletos` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `data` varchar(100) NOT NULL,
  `situacao` varchar(50) NOT NULL,
  `vencimento` varchar(100) NOT NULL,
  `chave` varchar(255) NOT NULL,
  `link` text NOT NULL,
  `descricao` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Fazendo dump de dados para tabela `boletos`
--

INSERT INTO `boletos` (`id`, `username`, `valor`, `data`, `situacao`, `vencimento`, `chave`, `link`, `descricao`) VALUES
(14, 'caique2016', '20000', '2016-04-27', 'Pendente', '2016-05-02', 'caique2016-yLUpup', 'https://visualizacaosandbox.gerencianet.com.br/emissao/92655_27_LEOCA8/A4XB-92655-40108-NAEBO8', 'Pacote'),
(15, 'caique20167', '100000', '2016-04-27', 'Pendente', '2016-05-02', 'caique20167-esU6yP', 'https://visualizacaosandbox.gerencianet.com.br/emissao/92655_28_BOSI2/A4XB-92655-40109-LELUA8', 'Pacote'),
(16, 'teste1', '100000', '2016-04-27', 'Pendente', '2016-05-02', 'teste1-AHApA8', 'https://visualizacao.gerencianet.com.br/emissao/92655_40_HIDA0/A4XB-92655-35489310-CALUA0', 'Pacote'),
(17, 'fabricaderenda', '100000', '2016-04-27', 'Pendente', '2016-05-02', 'fabricaderenda-DegYSe', 'https://visualizacao.gerencianet.com.br/emissao/92655_41_BRAHI4/A4XB-92655-35489528-TALE2', 'Pacote'),
(18, 'sucesso', '20000', '2016-04-28', 'Pendente', '2016-05-03', 'sucesso-uByJUW', 'https://visualizacao.gerencianet.com.br/emissao/92655_42_LAENA7/A4XB-92655-35535060-BOMA4', 'Pacote'),
(19, 'caique20189', '50000', '2016-04-28', 'Pendente', '2016-05-03', 'caique20189-8uDYhe', 'https://visualizacao.gerencianet.com.br/emissao/92655_43_SERHI1/A4XB-92655-35538827-HICOR2', 'Pacote'),
(20, 'sistemapoweron', '20000', '2016-04-28', 'Pendente', '2016-05-03', 'sistemapoweron-a8uHAM', 'https://visualizacao.gerencianet.com.br/emissao/92655_44_ZEDRO5/A4XB-92655-35539246-SERFO2', 'Pacote'),
(21, 'teste10', '20000', '2016-04-28', 'Pendente', '2016-05-03', 'teste10-gyTYWY', 'https://visualizacao.gerencianet.com.br/emissao/92655_45_SIMEH7/A4XB-92655-35540021-HILEO5', 'Pacote'),
(22, 'barachisio', '20000', '2016-04-28', 'Pendente', '2016-05-03', 'barachisio-gareSy', 'https://visualizacao.gerencianet.com.br/emissao/92655_46_MAMEH3/A4XB-92655-35540259-LELEO6', 'Pacote'),
(23, 'teste14', '20000', '2016-04-28', 'Pendente', '2016-05-03', 'teste14-AsyPyh', 'https://visualizacao.gerencianet.com.br/emissao/92655_47_LECA0/A4XB-92655-35540290-DORAA0', 'Pacote');

-- --------------------------------------------------------

--
-- Estrutura para tabela `cotas`
--

CREATE TABLE IF NOT EXISTS `cotas` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `valor` varchar(255) NOT NULL,
  `data` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Fazendo dump de dados para tabela `cotas`
--

INSERT INTO `cotas` (`id`, `valor`, `data`) VALUES
(1, '7.9', '16-04-25'),
(2, '8.25', '16-04-26'),
(3, '9.80', '16-04-27');

-- --------------------------------------------------------

--
-- Estrutura para tabela `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `code` text NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Fazendo dump de dados para tabela `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `comment`) VALUES
(1, 'US Dollar', 'USD', ''),
(2, 'Australian Dollar', 'AUD', ''),
(3, 'Brazilian Real', 'R$', ''),
(4, 'Canadian Dollar', 'CAD', ''),
(5, 'Czech Koruna', 'CZK', ''),
(6, 'Danish Krone', 'DKK', ''),
(7, 'Euro', 'EUR', ''),
(8, 'Thai Baht', 'THB', ''),
(9, 'Hong Kong Dollar', 'HKD', ''),
(10, 'Hungarian Forint', 'HUF', ''),
(11, 'Israeli New Sheqel', 'ILS', ''),
(12, 'Japanese Yen', 'JPY', ''),
(13, 'Malaysian Ringgit', 'MYR', ''),
(14, 'Mexican Peso', 'MXN', ''),
(15, 'Norwegian Krone', 'NOK', ''),
(16, 'New Zealand Dollar', 'NZD', ''),
(17, 'Philippine Peso', 'PHP', ''),
(18, 'Polish Zloty ', 'PLN', ''),
(19, 'Pound Sterling', 'GBP', ''),
(20, 'Russian Ruble', 'RUB', ''),
(21, 'Singapore Dollar', 'SGD', ''),
(22, 'Swedish Krona', 'SEK', ''),
(23, 'Swiss Franc', 'CHF', ''),
(24, 'Taiwan New Dollar', 'TWD', ''),
(26, 'Turkish Lira', 'TRY', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `emailtext`
--

CREATE TABLE IF NOT EXISTS `emailtext` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `etext` text NOT NULL,
  `emailactive` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Fazendo dump de dados para tabela `emailtext`
--

INSERT INTO `emailtext` (`id`, `code`, `etext`, `emailactive`) VALUES
(1, 'SIGNUP', 'OlÃ¡! Seja bem vindo.\r\nO seu cadastro foi efetuado com sucesso.\r\n\r\nObrigado.\r\nInovar - SoluÃ§Ãµes em Marketing de Rede.', 1),
(2, 'FORGOTPASSWORD', 'OlÃ¡! Por acaso vocÃª se esqueceu da sua senha?\r\nClique no link abaixo para recupera-la.\r\n\r\nObrigado.\r\nInovar - SoluÃ§Ãµes em Marketing de Rede.', 1),
(5, 'NEWMEMBER', 'You have got new order, bingo!', 1),
(6, 'NEWMEMBER', 'You have got new order, bingo!', 1),
(7, 'SIGNUP', 'OlÃ¡! Seja bem vindo.\r\nO seu cadastro foi efetuado com sucesso.\r\n\r\nObrigado.\r\nInovar - SoluÃ§Ãµes em Marketing de Rede.', 1),
(8, 'NEWMEMBER', 'You have got new order, bingo!', 1),
(9, 'SIGNUP', 'OlÃ¡! Seja bem vindo.\r\nO seu cadastro foi efetuado com sucesso.\r\n\r\nObrigado.\r\nInovar - SoluÃ§Ãµes em Marketing de Rede.', 1),
(10, 'NEWMEMBER', 'You have got new order, bingo!', 1),
(11, 'SIGNUP', 'OlÃ¡! Seja bem vindo.\r\nO seu cadastro foi efetuado com sucesso.\r\n\r\nObrigado.\r\nInovar - SoluÃ§Ãµes em Marketing de Rede.', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `extratos`
--

CREATE TABLE IF NOT EXISTS `extratos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `beneficiado` varchar(255) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `data` varchar(205) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `posteddate` date NOT NULL,
  `valid` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `currency` text NOT NULL,
  `details` text NOT NULL,
  `tax` double NOT NULL DEFAULT '0',
  `mpay` double NOT NULL DEFAULT '0',
  `sbonus` double DEFAULT '0',
  `cdate` date NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `level1` double NOT NULL DEFAULT '0',
  `level2` double NOT NULL DEFAULT '0',
  `level3` double NOT NULL DEFAULT '0',
  `level4` double NOT NULL DEFAULT '0',
  `level5` double NOT NULL DEFAULT '0',
  `level6` double NOT NULL DEFAULT '0',
  `level7` double NOT NULL DEFAULT '0',
  `level8` double NOT NULL DEFAULT '0',
  `level9` double NOT NULL DEFAULT '0',
  `level10` double NOT NULL DEFAULT '0',
  `level11` double NOT NULL DEFAULT '0',
  `level12` double NOT NULL DEFAULT '0',
  `level13` double NOT NULL DEFAULT '0',
  `level14` double NOT NULL DEFAULT '0',
  `level15` double NOT NULL DEFAULT '0',
  `level16` double NOT NULL DEFAULT '0',
  `level17` double NOT NULL DEFAULT '0',
  `level18` double NOT NULL DEFAULT '0',
  `level19` double NOT NULL DEFAULT '0',
  `level20` double NOT NULL DEFAULT '0',
  `gateway` int(1) NOT NULL DEFAULT '3',
  `validity` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Fazendo dump de dados para tabela `packages`
--

INSERT INTO `packages` (`id`, `name`, `price`, `currency`, `details`, `tax`, `mpay`, `sbonus`, `cdate`, `active`, `level1`, `level2`, `level3`, `level4`, `level5`, `level6`, `level7`, `level8`, `level9`, `level10`, `level11`, `level12`, `level13`, `level14`, `level15`, `level16`, `level17`, `level18`, `level19`, `level20`, `gateway`, `validity`) VALUES
(18, 'MF1', 200, 'R$', 'Pacote: MF1', 0, 200, 0, '2016-04-25', 1, 20, 10, 10, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 999999),
(19, 'MF2', 500, 'R$', 'Pacote: MF2', 0, 200, 0, '2016-04-25', 1, 50, 25, 25, 25, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 999999),
(20, 'MF3', 1000, 'R$', 'Pacote: MF3', 0, 200, 0, '2016-04-25', 1, 100, 50, 50, 50, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 999999);

-- --------------------------------------------------------

--
-- Estrutura para tabela `paymentgateway`
--

CREATE TABLE IF NOT EXISTS `paymentgateway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `comment` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Fazendo dump de dados para tabela `paymentgateway`
--

INSERT INTO `paymentgateway` (`id`, `Name`, `status`, `comment`, `date`) VALUES
(1, 'PayPal', 0, 0, '0000-00-00'),
(2, 'Cash On Delivery', 1, 0, '0000-00-00'),
(3, 'Payza', 0, 0, '0000-00-00'),
(4, 'SolidTrustPay', 0, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `userid` varchar(50) NOT NULL,
  `payment_amount` double NOT NULL DEFAULT '0',
  `payment_status` int(1) NOT NULL DEFAULT '0',
  `itemid` varchar(25) NOT NULL,
  `createdtime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Fazendo dump de dados para tabela `payments`
--

INSERT INTO `payments` (`id`, `userid`, `payment_amount`, `payment_status`, `itemid`, `createdtime`) VALUES
(22, '1', 700, 0, '', '2016-04-27 14:06:26'),
(23, '3', 200, 0, '', '2016-04-27 14:07:06'),
(24, '47', 300, 0, '', '2016-04-27 14:08:26'),
(25, '49', 200, 0, '', '2016-04-27 14:09:20'),
(26, '75', 200, 0, '', '2016-04-27 14:10:20'),
(27, '77', 200, 0, '', '2016-04-27 14:10:52');

-- --------------------------------------------------------

--
-- Estrutura para tabela `paypalpayments`
--

CREATE TABLE IF NOT EXISTS `paypalpayments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `transacid` text NOT NULL,
  `price` double DEFAULT '0',
  `currency` text NOT NULL,
  `date` date NOT NULL,
  `cod` int(1) NOT NULL DEFAULT '0',
  `renew` int(1) NOT NULL DEFAULT '0',
  `renacid` int(9) NOT NULL,
  `pckid` double NOT NULL,
  `gateway` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `email` varchar(100) NOT NULL DEFAULT 'Enter Your E-Mail Address',
  `sno` int(9) NOT NULL,
  `wlink` varchar(100) NOT NULL DEFAULT 'www.yourwebsite.com',
  `invoicedetails` text NOT NULL,
  `coname` text NOT NULL,
  `fblink` text NOT NULL,
  `twitterlink` text NOT NULL,
  `paypalid` text NOT NULL,
  `maintain` int(1) NOT NULL DEFAULT '0',
  `alwdpayment` int(11) NOT NULL DEFAULT '0',
  `minmobile` double NOT NULL,
  `maxmobile` double NOT NULL,
  `footer` varchar(50) NOT NULL,
  `header` varchar(50) NOT NULL,
  `payzaid` varchar(128) NOT NULL DEFAULT 'Not Available',
  `solidtrustid` varchar(128) NOT NULL DEFAULT 'Not Available',
  `solidbutton` varchar(128) NOT NULL DEFAULT 'Not Available',
  PRIMARY KEY (`sno`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `settings`
--

INSERT INTO `settings` (`email`, `sno`, `wlink`, `invoicedetails`, `coname`, `fblink`, `twitterlink`, `paypalid`, `maintain`, `alwdpayment`, `minmobile`, `maxmobile`, `footer`, `header`, `payzaid`, `solidtrustid`, `solidbutton`) VALUES
('contato@sistemapoweron.com', 0, 'http://sistemapoweron.com/', 'Brasil', 'Sistema PowerOn ', 'www.facebook.com', 'www.twitter.com', 'financeiro@sistemapoweron.com', 0, 1, 0, 0, 'Sistema PowerOn', '', 'financeiro@sistemapoweron.com', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
