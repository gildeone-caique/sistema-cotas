<div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">MENU DE OPÇÕES</div>
<ul class="nav nav-main" data-ride="collapse">
    <li class="active"> <a href="dashboard.php" class="auto"> <i class="i i-home icon"> </i> <span>Visão Geral</span> </a> </li>
    <li class="active"> <a href="profile.php" class="auto"> <i class="fa fa-user"> </i> <span>Meus dados</span> </a> </li>
    <li class="active"> <a href="downline.php" class="auto"> <i class="fa fa-exchange" aria-hidden="true"> </i> <span>Minha Rede</span> </a> </li>
    <li class="active"> <a href="paymentshistory.php" class="auto"> <i class="fa fa-money" aria-hidden="true"> </i> <span>Pagamentos</span> </a> </li>
    <li class="active"> <a href="faturas.php" class="auto"> <i class="i i-statistics icon"> </i> <span>Fatura da conta</span> </a> </li>
    <li class="active"> <a href="notifications.php" class="auto"> <i class="fa fa-tags" aria-hidden="true"> </i> <span>Notificações</span> </a> </li>
    <li class="active"> <a href="contact.php" class="auto"> <i class="fa fa-envelope-o"> </i> <span>Suporte</span> </a> </li>
    <li class="active"> <a href="extratos.php" class="auto"> <i class="fa fa-money"> </i> <span>Extratos</span> </a> </li>
    <li class="active"> <a href="upgrade.php" class="auto"> <i class="fa fa-upload"> </i> <span>Upgrade</span> </a> </li>
    <li> <a href="materiais.php" class="auto"><i class="fa fa-book" aria-hidden="true"></i> <span>Materiais </span> </a> </li>

</ul>
<div class="line dk hidden-nav-xs"></div>

