<?php

require __DIR__ . '/vendor/autoload.php';

class mercadopago {

    public function pagamentoUrl($userInfo, $pacoteInfo, $tipo = '') {
        $util = new util();
        $conexao = $util->conn;
        if ($tipo == '') {
            $tipo = 'Ativação';
        }if ($tipo == 1) {

            $tipo = 'Upgrade';
        }if ($tipo == 2) {

            $tipo = 'Renovação';
        }
        //RheGjHx6aBYmvC67
        $mp = new MP("3282728994378835", "t9NvBXeCjr17X099a7LWIhnsqh4fUCi5");
        $preference_data = array(
            "items" => array(
                array(
                    "title" => $tipo . '-' . $pacoteInfo['name'],
                    "quantity" => 1,
                    "currency_id" => "BRL",
                    "unit_price" => $pacoteInfo['price'],
                    'notification_url' => URL . '/RheGjHx6aBYmvC67.php',
                    "back_urls" => array('success' => URL . '/finalthankyou.php?status=1', 'pending' => URL . '/finalthankyou.php?status=2', 'failure' => URL . '/finalthankyou.php?status=3', 'pending' => URL . '/finalthankyou.php?status=4')
                )
            )
        );

        $preference = $mp->create_preference($preference_data);
        $ref = $preference['response']['id'];

        $url = $preference['response']['init_point'];
        if ($url == '') {
            $url = "javascript:alert('Opss,ocorreu um erro.Atualize a página novamente.');";
        } else {
            $url = $preference['response']['init_point'];
            $vencimento = date('Y-m-d', strtotime("+4 days"));
            $util->insertPagamento(['pacote'=>$pacoteInfo['id'],'vencimento' => $vencimento, 'valor' => $pacoteInfo['price'], 'url' => $url, 'status' => 1, 'ref' => $ref, 'username' => $userInfo['username'], 'data_criacao' => date('Y-m-d'), 'descricao' => $tipo . '-' . $pacoteInfo['name']]);
        }

        return $url;
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

