<?php

define('SITE_ROOT', realpath(dirname(__FILE__)));
include SITE_ROOT . '/mysql/MysqliDb.php';

class util {

    public $conn;
    public $host = 'localhost';
    public $senha = '81495203';
    public $dbname = 'ennixbrasil';
    public $dbuser = 'root';
    public $con1;

    function __construct() {

        $con = new mysqli($this->host, $this->dbuser, $this->senha, $this->dbname);
        if ($con->connect_errno) {
            echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        }
        $DB = new MysqliDb2($con);
        $this->conn = $DB;
        $this->con1 = $con;
    }

    public function insertTB($tb, $data) {
        $this->conn->insert($tb, $data);
    }

    public function getTable($tb) {
        return $this->conn->get($tb);
    }

    public function insertPagamento($data) {
        return $this->conn->insert('pagamentos', $data);
    }

    public function exibirErros() {
        error_reporting(E_ALL);
        ini_set("display_errors", 1);
    }

    public function validarAdmin() {

        if (!isset($_SESSION['adminidusername'])) {
            redirect('index.php');
        }
    }

    public function validarUser() {

        if (!isset($_SESSION['username'])) {
            redirect('index.php');
        }
    }

    function email($titulo, $mensagem) {
        $sqlquery = "SELECT wlink FROM settings where sno=0"; //fetching website from databse
        $rec2 = mysqli_query($this->con1, $sqlquery);
        $row2 = mysqli_fetch_row($rec2);
        $wlink = $row2[0]; //assigning website address

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <no-reply@' . $wlink . '>' . "\r\n";
        $to = $email;
        $subject = $titulo;
        $message = $mensagem;
        //mail($to, $subject, $message, $headers);
    }

    public function statusPagamento($id) {
        switch ($id) {
            case 1:
                $status = 'Pendete';
                break;
            case 3:
                $status = 'Pago';
                break;
            case 55:
                $status = 'Cancelada';
                break;
            default:
                $status = 'Desconhecido';

                break;
        }
        return $status;
    }

    public function maintain($id, $mensage) {
        $configs = $this->conn->getOne('settings');
        if ($id == $configs['maintain']) {
            $this->redirect('index.php?maintain=' . $mensage);
        }
    }

    public function faturasVencidasDel() {
        $hoje = date('Y-m-d');
        $this->conn->where('status', 1);
        $pagamentos = $this->conn->get('pagamentos');
        foreach ($pagamentos as $value) {
            $hojestr = strtotime($hoje);
            $vencimentostr = strtotime($value['vencimento']);
            if ($hojestr > $vencimentostr) {
                $this->conn->where('id', $value['id']);
                $this->conn->delete('pagamentos');
            }
        }
    }

    public function teste() {
        echo 'ok';
    }

    function redirect($url = 'index.php') {
        header("Location: $url");
        exit;
    }

    public function ativarUsr($usr) {
        $this->actionUsr($usr);
    }

    public function ativarUpgrade($usr, $pacote) {
        $this->actionUsr($usr, 1, $pacote);
    }

    public function ativarRenovacao($usr) {
        $this->actionUsr($usr, 2);
    }

    public function actionUsr($usr, $act = '', $pacote = 0) {
        $today = date('Y-m-d');

        /*
         * 
         * 
         * */

        $tomake = $usr;
        $con = $this->con1;
        $DB = $this->conn;
        if ($act == '') {
            $r12 = mysqli_query($con, "UPDATE affiliateuser SET active=1 WHERE username='$tomake'");
            $s12 = mysqli_query($con, "UPDATE affiliateuser SET launch=1 WHERE username='$tomake'");
            $desc = 'Ativação';
        } else if ($act == 1 and $pacote > 0) {
            $r12 = mysqli_query($con, "UPDATE affiliateuser SET pcktaken='$pacote' WHERE username='$tomake'");
            $desc = 'Upgrade';
        } else if ($act == 2) {
            $r12 = mysqli_query($con, "UPDATE affiliateuser SET active=1 WHERE username='$tomake'");
            $desc = 'Renovação';
        }


        if ($r12) {
//print "<center>Profile Activated<br/>Redirecting in 2 seconds...</center>";

            $query = "SELECT * FROM  affiliateuser where username='$tomake'";


            $result = mysqli_query($con, $query);
            $i = 0;
            while ($row = mysqli_fetch_array($result)) {

                $id = "$row[Id]";
                $username = "$row[username]";
                $fname = "$row[fname]";
                $email = "$row[email]";
                $mobile = "$row[mobile]";
                $active = "$row[active]";
                $doj = "$row[doj]";
                $country = "$row[country]";
                $ear = "$row[tamount]";
                $ref = "$row[referedby]";
                $pck = "$row[pcktaken]";
                if ($active == 1) {
                    $status = "Active/Paid";
                } else if ($active == 0) {
                    $status = "UnActive/Unpaid";
                } else {
                    $status = "Unknown";
                }
                $qu = "SELECT * FROM  packages where id = $pck";
                $re = mysqli_query($con, $qu);
                while ($r = mysqli_fetch_array($re)) {
                    $pckid = "$r[id]";
                    $pckname = "$r[name]";
                    $pckprice = "$r[price]";
                    $pckcur = "$r[currency]";
                    $pcksbonus = "$r[sbonus]";
                    $l1 = "$r[level1]";
                    $l2 = "$r[level2]";
                    $l3 = "$r[level3]";
                    $l4 = "$r[level4]";
                    $l5 = "$r[level5]";
                    $l6 = "$r[level6]";
                    $l7 = "$r[level7]";
                    $l8 = "$r[level8]";
                    $l9 = "$r[level9]";
                    $l10 = "$r[level10]";
                    $l11 = "$r[level11]";
                    $l12 = "$r[level12]";
                    $l13 = "$r[level13]";
                    $l14 = "$r[level14]";
                    $l15 = "$r[level15]";
                    $l16 = "$r[level16]";
                    $l17 = "$r[level17]";
                    $l18 = "$r[level18]";
                    $l19 = "$r[level19]";
                    $l20 = "$r[level20]";
                    $arr = array("$l1", "$l2", "$l3", "$l4", "$l5", "$l6", "$l7", "$l8", "$l9", "$l10", "$l11", "$l12", "$l13", "$l14", "$l15", "$l16", "$l17", "$l18", "$l19", "$l20");


                    $temp = $ref;
                    $s102 = mysqli_query($con, "UPDATE affiliateuser SET tamount=tamount+$pcksbonus WHERE username='$tomake'");
                    if ($pcksbonus > 0) {
                        $dataExtrato2['valor'] = $pcksbonus;
                        $dataExtrato2['beneficiado'] = $tomake;
                        $dataExtrato2['descricao'] = 'Bônus de ativação';
                        $dataExtrato2['usuario'] = $_SESSION['adminidusername'];
                        $dataExtrato2['data'] = $today;
                        $id = $DB->insert('extratos', $dataExtrato2);
                    }
                    $dataExtrato2['valor'] = $pckprice;
                    $dataExtrato2['beneficiado'] = $_SESSION['adminidusername'];
                    $dataExtrato2['descricao'] = $desc;
                    $dataExtrato2['usuario'] = $tomake;
                    $dataExtrato2['data'] = $today;
                    $id = $DB->insert('extratos', $dataExtrato2);
                    addSaldo($DB, $_SESSION['adminidusername'], $pckprice);
                    $r123 = mysqli_query($con, "UPDATE affiliateuser SET tamount= tamount+$arr[0] WHERE username='$temp'");
                    if ($arr[0] > 0) {
                        $dataExtrato2['valor'] = $arr[0];
                        $dataExtrato2['beneficiado'] = $temp;
                        $dataExtrato2['descricao'] = 'Indicação direita';
                        $dataExtrato2['usuario'] = $_SESSION['adminidusername'];
                        $dataExtrato2['data'] = $today;
                        $id = $DB->insert('extratos', $dataExtrato2);
                    }
                    for ($i = 1; $i < 20; $i++) {

                        $qexe = "SELECT * FROM  affiliateuser where username='$temp'";
                        $rexe = mysqli_query($con, $qexe);

                        while ($ress = mysqli_fetch_array($rexe)) {
                            $ans = "$ress[referedby]";
                        }
                        $r1234 = mysqli_query($con, "UPDATE affiliateuser SET tamount= tamount+$arr[$i] WHERE username='$ans'");
                        if ($arr[$i] > 0) {
                            $dataExtrato2['valor'] = $arr[0];
                            $dataExtrato2['beneficiado'] = $ans;
                            $dataExtrato2['descricao'] = 'Indicação Indireta';
                            $dataExtrato2['usuario'] = $_SESSION['adminidusername'];
                            $dataExtrato2['data'] = $today;
                            $id = $DB->insert('extratos', $dataExtrato2);
                            removeSaldo($DB, $_SESSION['adminidusername'], $arr[0], 'Indicação Indireta');
                        }
                        $temp = $ans;
                    }
                }
            }
        }
    }

    public function notificacaoBoasVindas($username) {
        $this->conn->insert('notifications', ['subject' => 'Seja Bem vindo(a)', 'body' => 'Possibilidade de envio de email sempre que houver alteração no pedido de saque, por ex. pago o sis envia email de saque pago, estorno email com pagamento recusado e valor estornado com opção de escrever o motivo do estorno
•	Campo para anotação de motivo de pagamento estornado no sistema', 'posteddate' => date('Y-m-d'), 'valid' => 1, 'status' => 0, 'user_id' => $username]);
    }

    public function configSite() {
        return $this->conn->getOne('settings');
    }

    public function dia($dia) {
        switch ($dia) {

            case 0: $semana = "Domingo";
                break;
            case 1: $semana = "Segunda Feira";
                break;
            case 2: $semana = "Terça Feira";
                break;
            case 3: $semana = "Quarta Feira";
                break;
            case 4: $semana = "Quinta Feira";
                break;
            case 5: $semana = "Sexta Feira";
                break;
            case 6: $semana = "Sábado";
                break;
        }
        return $semana;
    }

}
