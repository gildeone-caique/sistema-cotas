<?php

require __DIR__ . '/vendor/autoload.php';

use Iugu;

class IuguPay {

    public function pagamentoUrl($token, $userInfo, $pacoteInfo, $tipo = '') {
        Iugu::setApiKey($token);
        if ($tipo == '') {
            $tipo = 'Ativação';
        }if ($tipo == 1) {

            $tipo = 'Upgrade';
        }if ($tipo == 2) {

            $tipo = 'Renovação';
        }
        $vencimento = date('Y-m-d', strtotime("+5 days"));
        $this->invoice = \Iugu_Invoice::create(
                        Array(
                            "email" => $userInfo['email'],
                            "due_date" => $vencimento,
                            "items" =>
                            Array(
                                Array(
                                    "description" => $tipo . '-' . $pacoteInfo['name'],
                                    "quantity" => "1",
                                    "price_cents" => $pacoteInfo['price'] * 100
                                )
                            )
                        )
        );
        $retorno = $this->invoice;
        if (isset($retorno['secure_url'])) {

            return $retorno['secure_url'];
        } else {
            return false;
        }
    }

}

?>