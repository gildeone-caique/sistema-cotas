<?php

require __DIR__ . '/vendor/autoload.php';

use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

class GNetRepository {

    private $api;
    public $total;
    public $client_id;
    public $client_secret;

    /* public function __construct() {
      $this->api = new Gerencianet([
      'client_id' => 'Client_Id_5650b8459061de506cc06aff11e5dc56c44a1f15',
      'client_secret' => 'Client_Secret_9b3d3cb0e5282a34945211bc753868415bec984a',
      'sandbox' => true
      ]);
      } */

    public function __construct($client_id_gnt, $client_secret_gnt) {
        $this->api = new Gerencianet([
            'client_id' => $client_id_gnt,
            'client_secret' => $client_secret_gnt,
            'sandbox' => true
        ]);
    }

    function gerarSenha($tamanho = 9, $forca = 0) {
        $vogais = 'aeuy';
        $consoantes = 'bdghjmnpqrstvz';
        if ($forca >= 1) {
            $consoantes .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($forca >= 2) {
            $vogais .= "AEUY";
        }
        if ($forca >= 4) {
            $consoantes .= '23456789';
        }
        if ($forca >= 8) {
            $vogais .= '@#$%';
        }

        $senha = '';
        $alt = time() % 2;
        for ($i = 0; $i < $tamanho; $i++) {
            if ($alt == 1) {
                $senha .= $consoantes[(rand() % strlen($consoantes))];
                $alt = 0;
            } else {
                $senha .= $vogais[(rand() % strlen($vogais))];
                $alt = 1;
            }
        }
        return $senha;
    }

    public function createTransaction($pacote, $username, $DB) {
        $DB->where('id', $pacote);
        $pacote = $DB->getOne('packages');
        $custom = $username . '-' . $this->gerarSenha(6, 4);
        $this->total = $pacote['price'] * 100;
        $body = [
            'items' => [
                [
                    'name' => $pacote['name'],
                    'amount' => 1,
                    'value' => $pacote['price'] * 100
                ]
            ],
            'metadata' => [
                'custom_id' => $custom,
                'notification_url' => 'http://sistemapoweron.com/JVaCwrYdK9ELpg3M.php'
            ]
        ];

        return $this->api->createCharge([], $body);
    }

    public function createBoleto($id, $userId, $DB) {
        $DB->where('username', $userId);
        $user = $DB->getOne('affiliateuser');
        $params = [
            'id' => $id
        ];
        $vencimento = date('Y-m-d', strtotime("+5 days"));
        $body = [
            'payment' => [
                'banking_billet' => [
                    'expire_at' => strval($vencimento),
                    'customer' => [
                        'name' => $user['fname'],
                        'cpf' => str_replace(array('(', ')', '.', '-'), '', $user['cpf']),
                        'phone_number' => str_replace(array('(', ')', '.', '-'), '', $user['mobile'])
                    ]
                ]
            ]
        ];

        return $this->api->payCharge($params, $body);
    }

    public function gerarBoleto($data, $DB, $tipo = '') {
        $trans = $this->createTransaction($data['pcktaken'], $data['username'], $DB);
        $this->trans = $trans;
        $boleto = $this->createBoleto($trans['data']['charge_id'], $data['username'], $DB);
        $boletoData['chave'] = $trans['data']['custom_id'];
        $hoje = date('Y-m-d');
        $boletoData['data'] = $hoje;
        $boletoData['vencimento'] = date('Y-m-d', strtotime("+5 days"));
        $boletoData['valor'] = $this->total;
        $boletoData['situacao'] = 'Pendente';
        $boletoData['link'] = $boleto['data']['link'];
        $DB->where('id', $data['pcktaken']);
        $pacoteData = $DB->getOne('packages');
        if ($tipo == '') {
            $desc = 'Ativação - ' . $pacoteData['name'];
        }if ($tipo == 1) {
            $desc = 'Upgrade - ' . $pacoteData['name'];
        }if ($tipo == 1) {
            $desc = 'Renovação - ' . $pacoteData['name'];
        }
        $boletoData['descricao'] = 'Pacote';
        $boletoData['username'] = $data['username'];
        return $boleto['data']['link'];
    }

    public function notification($token) {
        $params = [
            'token' => $token
        ];

        try {
            $notification = $this->api->getNotification($params, []);
            return $notification;
        } catch (GerencianetException $e) {
            exit($e->getMessage() . ' - E:' . $e->getCode());
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }

}
